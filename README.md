[![Build Status](https://circleci.com/bb/murilocosta/gerador-boleto-cnab.png)](https://circleci.com/bb/murilocosta/gerador-boleto-cnab)

# CNAB – Arquivos de remessa e retorno de boletos bancários
___
Uma biblioteca para processamento de arquivos de remessa e retorno de boletos bancários seguindo os padrões CNAB 150, 240 e 400 definidos pelo [FEBRABAN](https://portal.febraban.org.br).

## Configuração do projeto para desenvolvimento
O projeto é compatível com Java JDK 7, as dependências para montar o projeto estão listadas abaixo:

1. Faça download e instalação do [Java JDK 7](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html)
2. Faça download e instalação do [Apache Maven 3.x](https://maven.apache.org/download.cgi)

Execute o comando abaixo na pasta raiz da aplicação:
```
mvn install
```

### Utilização da biblioteca

Inclua o cnab-boleto.jar no classpath de seu projeto e utilize a classe `GerenciadorProcessoFactory`, o gerenciador de processo, como no exemplo abaixo:
```java
try {
    File arquivoRetorno = new File("caminho/para/o/arquivo.ret");
    RetornoInterface retorno = GerenciadorProcessoFactory.getProcessadorArquivo(arquivoRetorno).processar();
    // Realiza operações com o retorno
} catch (IOException | RetornoArquivoException exception) {
    // Tratamento da exceção
}
```
O objeto `RetornoInterface` contem todas as informações do arquivo de retorno e é composto de: `CnabCabecalhoInterface`, `CnabDetalhamentoInterface` e `CnabTrailerInterface`, ou qualquer uma de suas implementações.

### Contribuição para o projeto

* Necessita mais testes de unidade;
* Revisão de código aplicando conceito DRY;
* Integração com o spring para habilitar injeção de dependência;
* Necessita mais comentários de documentação nas classes;
