package br.org.acropole.cnab.domain.cnab150;

import br.org.acropole.cnab.domain.GerenciadorProcessoAbstract;
import br.org.acropole.cnab.domain.cnab150.processador.Cnab150Processador;
import br.org.acropole.cnab.domain.cnab150.processador.Cnab150ProcessadorRetorno;

public class Cnab150GerenciadorProcesso extends GerenciadorProcessoAbstract<Cnab150Processador, Cnab150ProcessadorRetorno> {

    public Cnab150GerenciadorProcesso(Cnab150Processador processadorArquivo, Cnab150ProcessadorRetorno processadorRetorno) {
        super(processadorArquivo, processadorRetorno);
    }

}
