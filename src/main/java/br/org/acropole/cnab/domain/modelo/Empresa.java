package br.org.acropole.cnab.domain.modelo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;

import java.util.Collection;
import java.util.HashSet;

@Builder
@ToString
@EqualsAndHashCode
public class Empresa {

    @Getter
    @Setter
    private String codigo;

    @Getter
    @Setter
    private String tipoInscricao;

    @Getter
    @Setter
    private String numeroInscricao;

    @Getter
    @Setter
    private String nome;

    @Getter
    @Setter
    private Endereco endereco;

    @Getter
    @Setter
    private Banco banco;

    @Getter
    @Setter
    private Collection<String> usos;

    @Getter
    @Setter
    private Collection<String> reservados;

    public void adicionarReservado(String reservado) {
        this.reservados = (this.reservados == null) ? new HashSet<String>() : this.reservados;
        this.reservados.add(reservado);
    }

    public void removerReservado(String reservado) {
        this.reservados = (this.reservados == null) ? new HashSet<String>() : this.reservados;
        if (!this.reservados.contains(reservado)) {
            this.reservados.remove(reservado);
        }
    }

}
