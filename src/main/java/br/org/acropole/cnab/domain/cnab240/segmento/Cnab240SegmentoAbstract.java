package br.org.acropole.cnab.domain.cnab240.segmento;

import br.org.acropole.cnab.domain.cnab240.arquivo.Cnab240RegistroTipo;
import br.org.acropole.cnab.domain.cnab240.segmento.tipo.Cnab240SegmentoTipo;
import br.org.acropole.cnab.infra.DecodificadorLinhaAbstract;
import br.org.acropole.cnab.infra.util.DecodificadorLinhaUtils;

import java.util.Date;

import static br.org.acropole.cnab.domain.cnab240.arquivo.Cnab240RegistroTipo.getColunaTipo;
import static br.org.acropole.cnab.domain.cnab240.segmento.tipo.Cnab240SegmentoTipo.getSegmentoTipo;

public abstract class Cnab240SegmentoAbstract extends DecodificadorLinhaAbstract implements Cnab240SegmentoInterface {

    protected static final String FORMATO_DATE_PADRAO_CNAB240 = "ddMMyyyy";

    protected Cnab240RegistroTipo getRegistro(String str, int posicao, int quantidade) {
        return getColunaTipo(getString(str, posicao, quantidade));
    }

    protected Cnab240SegmentoTipo getSegmento(String str, int posicao, int quantidade) {
        return getSegmentoTipo(getString(str, posicao, quantidade));
    }

    @Override
    protected Date getDate(String str, int inicio, int quantidade) {
        return DecodificadorLinhaUtils.getDate(str, inicio, quantidade, FORMATO_DATE_PADRAO_CNAB240);
    }
}
