package br.org.acropole.cnab.domain.cnab150.modelo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;

import java.math.BigDecimal;
import java.util.Date;

@Builder
@ToString
@EqualsAndHashCode
public class Pagamento {

    @Getter
    @Setter
    protected Date dataPagamento;

    @Getter
    @Setter
    protected String formaPagamento;

    @Getter
    @Setter
    protected BigDecimal valorPagamento;

}
