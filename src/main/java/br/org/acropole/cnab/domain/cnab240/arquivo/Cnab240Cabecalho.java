package br.org.acropole.cnab.domain.cnab240.arquivo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Cnab240Cabecalho extends Cnab240CabecalhoAbstract {

    @Getter
    @Setter
    protected String codigoArquivo;

    @Getter
    @Setter
    protected String densidade;

    @Getter
    @Setter
    protected Date dataGeracao;

    @Getter
    @Setter
    protected String sequencialRetorno;

    public Cnab240Cabecalho(Cnab240CabecalhoBuilder builder) {
        super(builder);
        this.codigoArquivo = builder.codigoArquivo;
        this.densidade = builder.densidade;
        this.dataGeracao = builder.dataGeracao;
        this.sequencialRetorno = builder.sequencialRetorno;
    }

    public static class Cnab240CabecalhoBuilder extends Cnab240CabecalhoBuilderAbstract<Cnab240CabecalhoBuilder, Cnab240Cabecalho> {

        protected String codigoArquivo;
        protected String densidade;
        protected Date dataGeracao;
        protected String sequencialRetorno;

        public Cnab240CabecalhoBuilder codigoArquivo(String codigoArquivo) {
            this.codigoArquivo = codigoArquivo;
            return getThis();
        }

        public Cnab240CabecalhoBuilder densidade(String densidade) {
            this.densidade = densidade;
            return getThis();
        }

        public Cnab240CabecalhoBuilder dataGeracao(Date dataGeracao) {
            this.dataGeracao = dataGeracao;
            return getThis();
        }

        public Cnab240CabecalhoBuilder sequencialRetorno(String sequencialRetorno) {
            this.sequencialRetorno = sequencialRetorno;
            return getThis();
        }

        @Override
        public Cnab240CabecalhoBuilder getThis() {
            return this;
        }

        @Override
        public Cnab240Cabecalho build() {
            return new Cnab240Cabecalho(this);
        }

    }

}
