package br.org.acropole.cnab.domain.cnab240.arquivo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Cnab240Trailer extends Cnab240TrailerAbstract {

    @Getter
    @Setter
    protected Integer quantidadeLotes;

    @Getter
    @Setter
    protected Integer quantidadeRegistros;

    @Getter
    @Setter
    protected Integer quantidadeContasConciliadas;

    public Cnab240Trailer(Cnab240TrailerBuilder builder) {
        super(builder);
        this.quantidadeLotes = builder.quantidadeLotes;
        this.quantidadeRegistros = builder.quantidadeRegistros;
        this.quantidadeContasConciliadas = builder.quantidadeContasConciliadas;
    }

    public static class Cnab240TrailerBuilder extends Cnab240TrailerBuilderAbstract<Cnab240TrailerBuilder, Cnab240Trailer> {

        protected Integer quantidadeLotes;
        protected Integer quantidadeRegistros;
        protected Integer quantidadeContasConciliadas;

        public Cnab240TrailerBuilder quantidadeLotes(Integer quantidadeLotes) {
            this.quantidadeLotes = quantidadeLotes;
            return getThis();
        }

        public Cnab240TrailerBuilder quantidadeRegistros(Integer quantidadeRegistros) {
            this.quantidadeRegistros = quantidadeRegistros;
            return getThis();
        }

        public Cnab240TrailerBuilder quantidadeContasConciliadas(Integer quantidadeContasConciliadas) {
            this.quantidadeContasConciliadas = quantidadeContasConciliadas;
            return getThis();
        }

        @Override
        public Cnab240TrailerBuilder getThis() {
            return this;
        }

        @Override
        public Cnab240Trailer build() {
            return new Cnab240Trailer(this);
        }

    }

}
