package br.org.acropole.cnab.domain.modelo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;

import java.math.BigDecimal;
import java.util.Date;

@Builder
@ToString
@EqualsAndHashCode
public class Ocorrencia {

    @Getter
    @Setter
    private Integer codigo;

    @Getter
    @Setter
    private Date data;

    @Getter
    @Setter
    private BigDecimal valor;

    @Getter
    @Setter
    private String complemento;

}
