package br.org.acropole.cnab.domain.cnab400.convenio;

import br.org.acropole.cnab.domain.cnab400.arquivo.Cnab400DetalhamentoAbstract;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Collection;
import java.util.HashSet;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Cnab400DetalhamentoConvenio extends Cnab400DetalhamentoAbstract implements Cnab400DetalhamentoConvenioInterface {

    @Getter
    @Setter
    protected String convenio;

    @Getter
    @Setter
    protected String controle;

    @Getter
    @Setter
    protected String nossoNumero;

    @Getter
    @Setter
    protected String digitoVerificadorNossoNumero;

    @Getter
    @Setter
    protected String tipoCobranca;

    @Getter
    @Setter
    protected String tipoCobrancaCmd72;

    @Getter
    @Setter
    protected Integer diasCalculo;

    @Getter
    @Setter
    protected String natureza;

    @Getter
    @Setter
    protected String variacaoCarteira;

    @Getter
    @Setter
    protected String contaCaucao;

    @Getter
    @Setter
    protected String confirmacao;

    @Getter
    @Setter
    protected String prefixoTitulo;

    @Getter
    @Setter
    protected Collection<String> usoBanco = new HashSet<String>();

    @Override
    public void adicionarUsoBanco(String usoBanco) {
        this.usoBanco.add(usoBanco);
    }

    @Override
    public void excluirUsoBanco(String usoBanco) {
        if (this.usoBanco.contains(usoBanco)) {
            this.usoBanco.remove(usoBanco);
        }
    }

    public Cnab400DetalhamentoConvenio(Cnab400DetalhamentoConvenioBuilder builder) {
        super(builder);
        this.convenio = builder.convenio;
        this.controle = builder.controle;
        this.nossoNumero = builder.nossoNumero;
        this.digitoVerificadorNossoNumero = builder.digitoVerificadorNossoNumero;
        this.tipoCobranca = builder.tipoCobranca;
        this.tipoCobrancaCmd72 = builder.tipoCobrancaCmd72;
        this.diasCalculo = builder.diasCalculo;
        this.natureza = builder.natureza;
        this.variacaoCarteira = builder.variacaoCarteira;
        this.contaCaucao = builder.contaCaucao;
        this.confirmacao = builder.confirmacao;
        this.prefixoTitulo = builder.prefixoTitulo;
    }

    public static class Cnab400DetalhamentoConvenioBuilder
            extends Cnab400DetalhamentoBuilderAbstract<Cnab400DetalhamentoConvenioBuilder, Cnab400DetalhamentoConvenio> {

        protected String convenio;
        protected String controle;
        protected String nossoNumero;
        protected String digitoVerificadorNossoNumero;
        protected String tipoCobranca;
        protected String tipoCobrancaCmd72;
        protected Integer diasCalculo;
        protected String natureza;
        protected String variacaoCarteira;
        protected String contaCaucao;
        protected String confirmacao;
        protected String prefixoTitulo;

        public Cnab400DetalhamentoConvenioBuilder convenio(String convenio) {
            this.convenio = convenio;
            return getThis();
        }

        public Cnab400DetalhamentoConvenioBuilder controle(String controle) {
            this.controle = controle;
            return getThis();
        }

        public Cnab400DetalhamentoConvenioBuilder NossoNumero(String nossoNumero) {
            this.nossoNumero = nossoNumero;
            return getThis();
        }

        public Cnab400DetalhamentoConvenioBuilder DigitoVerificadorNossoNumero(String digitoVerificadorNossoNumero) {
            this.digitoVerificadorNossoNumero = digitoVerificadorNossoNumero;
            return getThis();
        }

        public Cnab400DetalhamentoConvenioBuilder TipoCobranca(String tipoCobranca) {
            this.tipoCobranca = tipoCobranca;
            return getThis();
        }

        public Cnab400DetalhamentoConvenioBuilder TipoCobrancaCmd72(String tipoCobrancaCmd72) {
            this.tipoCobrancaCmd72 = tipoCobrancaCmd72;
            return getThis();
        }

        public Cnab400DetalhamentoConvenioBuilder DiasCalculo(Integer diasCalculo) {
            this.diasCalculo = diasCalculo;
            return getThis();
        }

        public Cnab400DetalhamentoConvenioBuilder Natureza(String natureza) {
            this.natureza = natureza;
            return getThis();
        }

        public Cnab400DetalhamentoConvenioBuilder VariacaoCarteira(String variacaoCarteira) {
            this.variacaoCarteira = variacaoCarteira;
            return getThis();
        }

        public Cnab400DetalhamentoConvenioBuilder ContaCaucao(String contaCaucao) {
            this.contaCaucao = contaCaucao;
            return getThis();
        }

        public Cnab400DetalhamentoConvenioBuilder Confirmacao(String confirmacao) {
            this.confirmacao = confirmacao;
            return getThis();
        }

        public Cnab400DetalhamentoConvenioBuilder PrefixoTitulo(String prefixoTitulo) {
            this.prefixoTitulo = prefixoTitulo;
            return getThis();
        }

        @Override
        public Cnab400DetalhamentoConvenioBuilder getThis() {
            return this;
        }

        @Override
        public Cnab400DetalhamentoConvenio build() {
            return new Cnab400DetalhamentoConvenio(this);
        }
    }

}
