package br.org.acropole.cnab.domain.cnab400.convenio;

import br.org.acropole.cnab.domain.cnab400.arquivo.Cnab400DetalhamentoInterface;

import java.util.Collection;

public interface Cnab400DetalhamentoConvenioInterface extends Cnab400DetalhamentoInterface {

    String getConvenio();

    void setConvenio(String convenio);

    String getControle();

    void setControle(String controle);

    String getNossoNumero();

    void setNossoNumero(String nossoNumero);

    String getDigitoVerificadorNossoNumero();

    void setDigitoVerificadorNossoNumero(String digitoVerificadorNossoNumero);

    String getTipoCobranca();

    void setTipoCobranca(String tipoCobranca);

    String getTipoCobrancaCmd72();

    void setTipoCobrancaCmd72(String tipoCobrancaCmd72);

    Integer getDiasCalculo();

    void setDiasCalculo(Integer diasCalculo);

    String getNatureza();

    void setNatureza(String natureza);

    String getVariacaoCarteira();

    void setVariacaoCarteira(String variacaoCarteira);

    String getContaCaucao();

    void setContaCaucao(String contaCaucao);

    String getConfirmacao();

    void setConfirmacao(String confirmacao);

    String getPrefixoTitulo();

    void setPrefixoTitulo(String prefixoTitulo);

    Collection<String> getUsoBanco();

    void setUsoBanco(Collection<String> $usoBanco);

    void adicionarUsoBanco(String usoBanco);

    void excluirUsoBanco(String usoBanco);

}
