package br.org.acropole.cnab.domain.cnab150.modelo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;

import java.math.BigDecimal;

@Builder
@ToString
@EqualsAndHashCode
public class Movimentacao {

    @Getter
    @Setter
    protected Pagamento pagamento;

    @Getter
    @Setter
    protected Recebimento recebimento;

    @Getter
    @Setter
    protected BigDecimal valorTitulo;

}
