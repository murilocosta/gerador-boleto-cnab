package br.org.acropole.cnab.domain.modelo;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Builder;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Cedente extends Pessoa {

    @Builder
    public Cedente(String nome, Banco banco) {
        super(nome, banco);
    }

}
