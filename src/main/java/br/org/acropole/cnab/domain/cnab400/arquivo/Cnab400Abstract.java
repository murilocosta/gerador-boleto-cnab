package br.org.acropole.cnab.domain.cnab400.arquivo;

import br.org.acropole.cnab.infra.arquivo.CnabAbstract;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public abstract class Cnab400Abstract extends CnabAbstract {

    public <B extends Cnab400BuilderAbstract<B, E>, E extends Cnab400Abstract> Cnab400Abstract(B builder) {
        super(builder);
    }

    public abstract static class Cnab400BuilderAbstract<B extends Cnab400BuilderAbstract<B, E>, E extends Cnab400Abstract>
            extends CnabBuilderAbstract<B, E> {

    }

}
