package br.org.acropole.cnab.domain.cnab400.processador;

import br.org.acropole.cnab.domain.cnab400.arquivo.Cnab400CabecalhoAbstract.Cnab400CabecalhoBuilderAbstract;
import br.org.acropole.cnab.domain.cnab400.arquivo.Cnab400CabecalhoInterface;
import br.org.acropole.cnab.domain.cnab400.arquivo.Cnab400DetalhamentoAbstract.Cnab400DetalhamentoBuilderAbstract;
import br.org.acropole.cnab.domain.cnab400.arquivo.Cnab400DetalhamentoInterface;
import br.org.acropole.cnab.domain.cnab400.arquivo.Cnab400RegistroTipo;
import br.org.acropole.cnab.domain.cnab400.arquivo.Cnab400Trailer.Cnab400TrailerBuilder;
import br.org.acropole.cnab.domain.cnab400.arquivo.Cnab400TrailerInterface;
import br.org.acropole.cnab.domain.modelo.Banco;
import br.org.acropole.cnab.domain.modelo.Cedente;
import br.org.acropole.cnab.domain.modelo.Cobranca;
import br.org.acropole.cnab.infra.ProcessadorArquivoAbstract;
import br.org.acropole.cnab.infra.arquivo.CnabInterface;

import java.io.File;
import java.util.Date;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Classe abstrata para leitura de arquivos de retorno de cobranças no padrão CNAB400/CBR643.<br/>
 * Layout Padrão CNAB/Febraban 400 posições baseado na documentação para:<br/>
 * "Layout de Arquivo Retorno para Convênios na faixa numérica entre 000.001 a 999.999 (Convênios de até 6 posições). Versão Set/09"<br/>
 * "Layout de Arquivo Retorno para convênios na faixa numérica entre 1.000.000 a 9.999.999 (Convênios de 7 posições). Versão Set/09"<br/>
 * do Banco do Brasil (arquivos Doc8826BR643Pos6.pdf e Doc2628CBR643Pos7.pdf)
 */
public abstract class Cnab400ProcessadorAbstract extends ProcessadorArquivoAbstract {

    protected static final String FORMATO_DATE_PADRAO_CNAB400 = "ddMMyy";

    public Cnab400ProcessadorAbstract(File arquivo) {
        super(arquivo);
    }

    protected abstract Cnab400CabecalhoInterface processarCabecalhoArquivo(String linha);

    protected abstract Cnab400DetalhamentoInterface processadorDetalhamentoArquivo(String linha);

    protected abstract Cnab400RegistroTipo getDetalhamentoTipo();

    protected Cnab400CabecalhoInterface processarCabecalhoArquivo(String linha, Cnab400CabecalhoBuilderAbstract builder) {
        String codigoNomeBanco = getString(linha, 77, 18);
        Matcher matcher = Pattern.compile("^([\\d]{3})(.+)").matcher(codigoNomeBanco);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Banco inválido!");
        }
        MatchResult matchResult = matcher.toMatchResult();
        Banco bancoCedente = Banco.builder().codigo(matchResult.group(1).trim())
                .nome(matchResult.group(2).trim())
                .agencia(getString(linha, 27, 4))
                .digitoVerificadorAgencia(getString(linha, 31, 1))
                .conta(getString(linha, 32, 8))
                .digitoVerificadorConta(getString(linha, 40, 1))
                .build();
        builder.registro(getRegistro(linha, 1, 1));
        builder.tipoOperacao(getString(linha, 2, 1))
                .identificadorTipoOperacao(getString(linha, 3, 7))
                .identificadorTipoServico(getString(linha, 10, 2))
                .tipoServico(getString(linha, 12, 8))
                .complemento(getString(linha, 20, 7));
        builder.cedente(Cedente.builder().banco(bancoCedente).nome(getString(linha, 47, 30)).build());
        builder.dataGravacao(getDate(linha, 95, 6))
                .sequencialRegistro(getString(linha, 395, 6));

        return Cnab400CabecalhoInterface.class.cast(builder.build());
    }

    protected Cnab400DetalhamentoInterface processadorDetalhamentoArquivo(String linha, Cnab400DetalhamentoBuilderAbstract builder) {
        Banco bancoEmissor = Banco.builder()
                .agencia(getString(linha, 22, 1))
                .digitoVerificadorAgencia(getString(linha, 31, 1))
                .conta(getString(linha, 23, 8))
                .digitoVerificadorConta(getString(linha, 31, 1))
                .build();
        Banco bancoRecebedor = Banco.builder()
                .codigo(getString(linha, 166, 3))
                .agencia(getString(linha, 169, 4))
                .digitoVerificadorAgencia(getString(linha, 173, 1))
                .build();
        builder.registro(getRegistro(linha, 1, 1));
        builder.bancoEmissor(bancoEmissor)
                .bancoRecebedor(bancoRecebedor)
                .taxaDesconto(getDecimal(linha, 96, 5))
                .taxaIof(getDecimal(linha, 101, 5))
                .carteira(getString(linha, 107, 2))
                .comando(getString(linha, 109, 2))
                .dataOcorrencia(getDate(linha, 111, 6))
                .numeroTitulo(getString(linha, 117, 10))
                .dataVencimento(getDate(linha, 147, 6))
                .valor(getDecimal(linha, 153, 13))
                .especie(getString(linha, 174, 2))
                .dataCredito(getDate(linha, 176, 6))
                .valorTarifa(getDecimal(linha, 182, 7))
                .outrasDespesas(getDecimal(linha, 189, 13))
                .jurosDesconto(getDecimal(linha, 202, 13))
                .descontoIof(getDecimal(linha, 215, 13))
                .valorAbatimento(getDecimal(linha, 228, 13))
                .descontoConcedido(getDecimal(linha, 241, 13))
                .valorRecebido(getDecimal(linha, 254, 13))
                .jurosMora(getDecimal(linha, 267, 13))
                .outrosRecebimentos(getDecimal(linha, 280, 13))
                .abatimentoNaoAprovado(getString(linha, 293, 13))
                .valorLancamento(getDecimal(linha, 306, 13))
                .indicativoDc(getString(linha, 319, 1))
                .indicadorValor(getString(linha, 320, 1))
                .valorAjuste(getDecimal(linha, 321, 12))
                .canalPagamentoTitulo(getString(linha, 393, 2))
                .sequencial(getString(linha, 395, 6));

        return Cnab400DetalhamentoInterface.class.cast(builder.build());
    }

    protected Cnab400TrailerInterface processarTrailerArquivo(String linha) {
        return new Cnab400TrailerBuilder()
                .registro(getRegistro(linha, 1, 1))
                .retorno(getString(linha, 2, 1))
                .tipoRegistro(getString(linha, 3, 2))
                .banco(Banco.builder().nome(getString(linha, 5, 3)).build())
                .simples(Cobranca.builder()
                        .quantidadeTitulos(getString(linha, 18, 8))
                        .valorTotal(getDecimal(linha, 26, 14))
                        .numeroAviso(getString(linha, 40, 8))
                        .build())
                .vinculada(Cobranca.builder()
                        .quantidadeTitulos(getString(linha, 58, 8))
                        .valorTotal(getDecimal(linha, 66, 14))
                        .numeroAviso(getString(linha, 80, 8))
                        .build())
                .caucionada(Cobranca.builder()
                        .quantidadeTitulos(getString(linha, 98, 8))
                        .valorTotal(getDecimal(linha, 106, 14))
                        .numeroAviso(getString(linha, 120, 8))
                        .build())
                .descontada(Cobranca.builder()
                        .quantidadeTitulos(getString(linha, 138, 8))
                        .valorTotal(getDecimal(linha, 146, 14))
                        .numeroAviso(getString(linha, 160, 8))
                        .build())
                .vendor(Cobranca.builder()
                        .quantidadeTitulos(getString(linha, 218, 8))
                        .valorTotal(getDecimal(linha, 226, 14))
                        .numeroAviso(getString(linha, 240, 8))
                        .build())
                .sequencial(getString(linha, 395, 6))
                .build();
    }

    @Override
    public CnabInterface processarLinha(String linha) {
        // É necessário adicionar um espaço vazio no início da linha para que possamos
        // trabalhar com índices iniciando com 1 e assim, obter os valores dos campos
        // nas posições exatas definidas pelo manual CNAB400
        String linhaTratada = tratarLinhaParaProcessamento(linha);

        Cnab400RegistroTipo tipo = getRegistro(linhaTratada, 1, 1);
        if (Cnab400RegistroTipo.CABECALHO.equals(tipo)) {
            CnabInterface processado = processarCabecalhoArquivo(linhaTratada);
            processado.criarLote();
            return processado;
        } else if (getDetalhamentoTipo().equals(tipo)) {
            return processadorDetalhamentoArquivo(linhaTratada);
        } else if (Cnab400RegistroTipo.TRAILER.equals(tipo)) {
            return processarTrailerArquivo(linhaTratada);
        } else {
            return null;
        }
    }

    protected Cnab400RegistroTipo getRegistro(String str, int inicio, int quantidade) {
        return Cnab400RegistroTipo.getRegistroTipo(getString(str, inicio, quantidade));
    }

    protected Date getDate(String str, int inicio, int quantidade) {
        return getDate(str, inicio, quantidade, FORMATO_DATE_PADRAO_CNAB400);
    }

}
