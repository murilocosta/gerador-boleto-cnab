package br.org.acropole.cnab.domain.modelo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;

@Builder
@ToString
@EqualsAndHashCode
public class Endereco {

    @Getter
    @Setter
    private String logradouro;

    @Getter
    @Setter
    private String numero;

    @Getter
    @Setter
    private String complemento;

    @Getter
    @Setter
    private String cep;

    @Getter
    @Setter
    private String complementoCep;

    @Getter
    @Setter
    private String cidade;

    @Getter
    @Setter
    private String estado;

}
