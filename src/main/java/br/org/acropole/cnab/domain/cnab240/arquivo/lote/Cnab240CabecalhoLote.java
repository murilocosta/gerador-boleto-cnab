package br.org.acropole.cnab.domain.cnab240.arquivo.lote;

import br.org.acropole.cnab.domain.cnab240.arquivo.Cnab240CabecalhoAbstract;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Collection;
import java.util.HashSet;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Cnab240CabecalhoLote extends Cnab240CabecalhoAbstract {

    @Getter
    @Setter
    protected String operacao;

    @Getter
    @Setter
    protected String servico;

    @Getter
    @Setter
    protected String formaLancamento;

    @Getter
    @Setter
    protected Collection<String> mensagens;

    public Cnab240CabecalhoLote(Cnab240CabecalhoLoteBuilder builder) {
        super(builder);
        this.operacao = builder.operacao;
        this.servico = builder.servico;
        this.formaLancamento = builder.formaLancamento;
        this.mensagens = builder.mensagens;
    }

    public void adicionarMensagem(String mensagem) {
        this.mensagens.add(mensagem);
    }

    public void excluirMensagem(String mensagem) {
        if (!this.mensagens.contains(mensagem)) {
            this.mensagens.remove(mensagem);
        }
    }

    public static class Cnab240CabecalhoLoteBuilder extends Cnab240CabecalhoBuilderAbstract<Cnab240CabecalhoLoteBuilder, Cnab240CabecalhoLote> {

        protected String operacao;
        protected String servico;
        protected String formaLancamento;
        protected Collection<String> mensagens = new HashSet<String>();

        public Cnab240CabecalhoLoteBuilder operacao(String operacao) {
            this.operacao = operacao;
            return getThis();
        }

        public Cnab240CabecalhoLoteBuilder servico(String servico) {
            this.servico = servico;
            return getThis();
        }

        public Cnab240CabecalhoLoteBuilder formaLancamento(String formaLancamento) {
            this.formaLancamento = formaLancamento;
            return getThis();
        }

        public Cnab240CabecalhoLoteBuilder mensagens(Collection<String> mensagens) {
            this.mensagens = mensagens;
            return getThis();
        }

        public Cnab240CabecalhoLoteBuilder mensagem(String mensagem) {
            this.mensagens.add(mensagem);
            return getThis();
        }

        @Override
        public Cnab240CabecalhoLoteBuilder getThis() {
            return this;
        }

        @Override
        public Cnab240CabecalhoLote build() {
            return new Cnab240CabecalhoLote(this);
        }

    }

}
