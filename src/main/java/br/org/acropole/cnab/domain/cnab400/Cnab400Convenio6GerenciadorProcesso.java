package br.org.acropole.cnab.domain.cnab400;

import br.org.acropole.cnab.domain.GerenciadorProcessoAbstract;
import br.org.acropole.cnab.domain.cnab400.processador.Cnab400Convenio6Processador;
import br.org.acropole.cnab.domain.cnab400.processador.Cnab400ProcessadorRetorno;

public class Cnab400Convenio6GerenciadorProcesso extends GerenciadorProcessoAbstract<Cnab400Convenio6Processador, Cnab400ProcessadorRetorno> {

    public Cnab400Convenio6GerenciadorProcesso(Cnab400Convenio6Processador processadorArquivo, Cnab400ProcessadorRetorno processadorRetorno) {
        super(processadorArquivo, processadorRetorno);
    }

}
