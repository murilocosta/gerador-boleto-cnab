package br.org.acropole.cnab.domain.cnab240.processador;

import br.org.acropole.cnab.domain.cnab240.arquivo.Cnab240Cabecalho;
import br.org.acropole.cnab.domain.cnab240.arquivo.Cnab240Cabecalho.Cnab240CabecalhoBuilder;
import br.org.acropole.cnab.domain.cnab240.arquivo.Cnab240Detalhamento;
import br.org.acropole.cnab.domain.cnab240.arquivo.Cnab240RegistroTipo;
import br.org.acropole.cnab.domain.cnab240.arquivo.Cnab240Trailer;
import br.org.acropole.cnab.domain.cnab240.arquivo.Cnab240Trailer.Cnab240TrailerBuilder;
import br.org.acropole.cnab.domain.cnab240.arquivo.lote.Cnab240CabecalhoLote;
import br.org.acropole.cnab.domain.cnab240.arquivo.lote.Cnab240CabecalhoLote.Cnab240CabecalhoLoteBuilder;
import br.org.acropole.cnab.domain.cnab240.arquivo.lote.Cnab240TrailerLote;
import br.org.acropole.cnab.domain.cnab240.arquivo.lote.Cnab240TrailerLote.Cnab240TrailerLoteBuilder;
import br.org.acropole.cnab.domain.cnab240.segmento.Cnab240SegmentoFactory;
import br.org.acropole.cnab.domain.modelo.Banco;
import br.org.acropole.cnab.domain.modelo.Cedente;
import br.org.acropole.cnab.domain.modelo.Empresa;
import br.org.acropole.cnab.domain.modelo.Endereco;
import br.org.acropole.cnab.infra.ProcessadorArquivoAbstract;
import br.org.acropole.cnab.infra.arquivo.CnabInterface;

import java.io.File;

import static br.org.acropole.cnab.domain.cnab240.arquivo.Cnab240RegistroTipo.getColunaTipo;
import static br.org.acropole.cnab.domain.cnab240.segmento.tipo.Cnab240SegmentoTipo.getSegmentoTipo;
import static br.org.acropole.cnab.infra.util.CollectionUtils.criarColecaoComElemento;

/**
 * Classe para leitura de arquivos de retorno para cobranças padrão CNAB240.<br/>
 * Layout Padrão Febraban 240 posições V08.4 de 01/09/2009 disponível em <a href="http://www.febraban.org.br">Febraban</a>
 */
public class Cnab240Processador extends ProcessadorArquivoAbstract {

    public Cnab240Processador(File arquivo) {
        super(arquivo);
    }

    protected Cnab240Cabecalho processarCabecalhoArquivo(String linha) {
        return new Cnab240CabecalhoBuilder()
                .cedente(Cedente.builder()
                        .banco(Banco.builder()
                                .codigo(getString(linha, 1, 3))
                                .agencia(getString(linha, 53, 5))
                                .digitoVerificadorAgencia(getString(linha, 58, 1))
                                .conta(getString(linha, 59, 12))
                                .digitoVerificadorConta(getString(linha, 71, 1))
                                .digitoVerificadorAgenciaConta(getString(linha, 72, 1))
                                .nome(getString(linha, 103, 30))
                                .reservados(criarColecaoComElemento(getString(linha, 172, 20)))
                                .build())
                        .build())
                .lote(getString(linha, 4, 4))
                .registro(getRegistro(linha, 8, 1))
                .cnab(getString(linha, 9, 9))
                .empresa(Empresa.builder()
                        .tipoInscricao(getString(linha, 18, 1))
                        .numeroInscricao(getString(linha, 19, 14))
                        .nome(getString(linha, 73, 30))
                        .reservados(criarColecaoComElemento(getString(linha, 192, 20)))
                        .build())
                .convenio(getString(linha, 33, 20))
                .cnab(getString(linha, 133, 10))
                .codigoArquivo(getString(linha, 143, 1))
                .dataGeracao(getDateTime(linha, 144, 14))
                .sequencialRetorno(getString(linha, 158, 6))
                .versaoLayout(getString(linha, 164, 3))
                .densidade(getString(linha, 167, 5))
                .cnab(getString(linha, 212, 29))
                .build();
    }

    protected Cnab240CabecalhoLote processarCabecalhoLote(String linha) {
        return new Cnab240CabecalhoLoteBuilder()
                .registro(getRegistro(linha, 8, 1))
                .lote(getString(linha, 4, 4))
                .operacao(getString(linha, 9, 1))
                .servico(getString(linha, 10, 2))
                .formaLancamento(getString(linha, 12, 2))
                .versaoLayout(getString(linha, 14, 3))
                .cnab(getString(linha, 17, 1))
                .convenio(getString(linha, 33, 20))
                .mensagem(getString(linha, 103, 40))
                .empresa(Empresa.builder()
                        .tipoInscricao(getString(linha, 18, 1))
                        .numeroInscricao(getString(linha, 19, 14))
                        .nome(getString(linha, 73, 30))
                        .endereco(
                                Endereco.builder()
                                        .logradouro(getString(linha, 143, 30))
                                        .numero(getString(linha, 173, 5))
                                        .complemento(getString(linha, 178, 15))
                                        .cidade(getString(linha, 193, 20))
                                        .cep(getString(linha, 213, 5))
                                        .complementoCep(getString(linha, 218, 3))
                                        .estado(getString(linha, 221, 2))
                                        .build())
                        .build())
                .cnab(getString(linha, 223, 8))
                .ocorrencia(getString(linha, 231, 10))
                .build();
    }

    protected Cnab240Detalhamento processarDetalhamentoArquivo(String linha) {
        String segmento = getString(linha, 14, 1);
        return new Cnab240SegmentoFactory().getSegmento(getSegmentoTipo(segmento)).montarDetalhamento(linha);
    }

    protected Cnab240Trailer processarTrailerArquivo(String linha) {
        return new Cnab240TrailerBuilder()
                .cedente(Cedente.builder()
                        .banco(Banco.builder()
                                .codigo(getString(linha, 1, 3))
                                .build())
                        .nome(getString(linha, 62, 30))
                        .build())
                .lote(getString(linha, 4, 4))
                .registro(getRegistro(linha, 8, 1))
                .cnab(getString(linha, 9, 9))
                .quantidadeLotes(getInteger(linha, 18, 6))
                .quantidadeRegistros(getInteger(linha, 24, 6))
                .quantidadeContasConciliadas(getInteger(linha, 30, 6))
                .cnab(getString(linha, 36, 205))
                .build();
    }

    protected Cnab240TrailerLote processarTrailerLote(String linha) {
        return new Cnab240TrailerLoteBuilder()
                .cedente(Cedente.builder()
                        .banco(Banco.builder()
                                .codigo(getString(linha, 1, 3))
                                .build())
                        .nome(getString(linha, 62, 30))
                        .build())
                .lote(getString(linha, 4, 4))
                .registro(getRegistro(linha, 8, 1))
                .cnab(getString(linha, 9, 9))
                .quantidadeRegistros(getInteger(linha, 18, 6))
                .valor(getDecimal(linha, 24, 16))
                .quantidadeMoedas(getDecimal(linha, 42, 13, 5))
                .numeroAvisoDebito(getString(linha, 60, 6))
                .cnab(getString(linha, 66, 165))
                .ocorrencia(getString(linha, 231, 10))
                .build();
    }

    @Override
    public CnabInterface processarLinha(String linha) {
        // É necessário adicionar um espaço vazio no início da linha para que possamos
        // trabalhar com índices iniciando com 1 e assim, obter os valores dos campos
        // nas posições exatas definidas pelo manual CNAB240
        String linhaTratada = tratarLinhaParaProcessamento(linha);

        Cnab240RegistroTipo colunaTipo = getRegistro(linhaTratada, 8, 1);
        switch (colunaTipo) {
        case CABECALHO:
            return processarCabecalhoArquivo(linhaTratada);

        case CABECALHO_LOTE:
            CnabInterface secaoProcessada = processarCabecalhoLote(linhaTratada);
            secaoProcessada.criarLote();
            return secaoProcessada;

        case DETALHAMENTO:
            return processarDetalhamentoArquivo(linhaTratada);

        case TRAILER:
            return processarTrailerArquivo(linhaTratada);

        case TRAILER_LOTE:
            return processarTrailerLote(linhaTratada);

        default:
            return null;
        }
    }

    protected Cnab240RegistroTipo getRegistro(String str, int posicao, int quantidade) {
        return getColunaTipo(getString(str, posicao, quantidade));
    }

}
