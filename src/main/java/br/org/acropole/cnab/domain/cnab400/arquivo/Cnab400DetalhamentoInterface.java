package br.org.acropole.cnab.domain.cnab400.arquivo;

import br.org.acropole.cnab.domain.modelo.Banco;
import br.org.acropole.cnab.domain.modelo.Cedente;
import br.org.acropole.cnab.infra.arquivo.CnabDetalhamentoInterface;

import java.math.BigDecimal;
import java.util.Date;

public interface Cnab400DetalhamentoInterface extends CnabDetalhamentoInterface {

    BigDecimal getTaxaDesconto();

    void setTaxaDesconto(BigDecimal taxaDesconto);

    BigDecimal getTaxaIof();

    void setTaxaIof(BigDecimal taxaIof);

    String getCarteira();

    void setCarteira(String cateira);

    String getComando();

    void setComando(String comando);

    Date getDataOcorrencia();

    void setDataOcorrencia(Date dataOcorrencia);

    String getNumeroTitulo();

    void setNumeroTitulo(String numeroTitulo);

    Date getDataVencimento();

    void setDataVencimento(Date dataVencimento);

    BigDecimal getValor();

    void setValor(BigDecimal valor);

    String getEspecie();

    void setEspecie(String especie);

    Date getDataCredito();

    void setDataCredito(Date dataCredito);

    BigDecimal getValorTarifa();

    void setValorTarifa(BigDecimal valorTarifa);

    BigDecimal getOutrasDespesas();

    void setOutrasDespesas(BigDecimal outrasDespesas);

    BigDecimal getJurosDesconto();

    void setJurosDesconto(BigDecimal jurosDesconto);

    BigDecimal getDescontoIof();

    void setDescontoIof(BigDecimal descontoIof);

    BigDecimal getValorAbatimento();

    void setValorAbatimento(BigDecimal valorAbatimento);

    BigDecimal getDescontoConcedido();

    void setDescontoConcedido(BigDecimal descontoConcedido);

    BigDecimal getValorRecebido();

    void setValorRecebido(BigDecimal valorRecebido);

    BigDecimal getJurosMora();

    void setJurosMora(BigDecimal jurosMora);

    BigDecimal getOutrosRecebimentos();

    void setOutrosRecebimentos(BigDecimal outrosRecebimentos);

    String getAbatimentoNaoAprovado();

    void setAbatimentoNaoAprovado(String abatimentoNaoAprovado);

    BigDecimal getValorLancamento();

    void setValorLancamento(BigDecimal valorLancamento);

    String getIndicativoDc();

    void setIndicativoDc(String indicativoDc);

    BigDecimal getValorAjuste();

    void setValorAjuste(BigDecimal valorAjuste);

    String getCanalPagamentoTitulo();

    void setCanalPagamentoTitulo(String canalPagTitulo);

    String getSequencial();

    void setSequencial(String sequencial);

    Banco getBancoEmissor();

    void setBancoEmissor(Banco bancoEmissor);

    Cedente getCedente();

    void setCedente(Cedente cedente);

    Banco getBancoRecebedor();

    void setBancoRecebedor(Banco bancoRecebedor);

    String getIndicadorValor();

    void setIndicadorValor(String indicadorValor);

}
