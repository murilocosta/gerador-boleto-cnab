package br.org.acropole.cnab.domain.cnab400.arquivo;

import br.org.acropole.cnab.domain.modelo.Banco;
import br.org.acropole.cnab.domain.modelo.Cobranca;
import br.org.acropole.cnab.infra.arquivo.CnabRegistroTipo;
import br.org.acropole.cnab.infra.arquivo.CnabTrailerInterface;

public interface Cnab400TrailerInterface extends CnabTrailerInterface {

    String getRetorno();

    void setRetorno(String retorno);

    CnabRegistroTipo getRegistro();

    void setRegistro(CnabRegistroTipo registro);

    String getSequencial();

    void setSequencial(String sequencial);

    Banco getBanco();

    void setBanco(Banco $banco);

    Cobranca getSimples();

    void setSimples(Cobranca $simples);

    Cobranca getVinculada();

    void setVinculada(Cobranca $vinculada);

    Cobranca getCaucionada();

    void setCaucionada(Cobranca $caucionada);

    Cobranca getVendor();

    void setVendor(Cobranca $vendor);

    Cobranca getDescontada();

    void setDescontada(Cobranca $descontada);

}
