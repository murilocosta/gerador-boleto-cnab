package br.org.acropole.cnab.domain.cnab150.processador;

import br.org.acropole.cnab.domain.cnab150.arquivo.Cnab150RegistroTipo;
import br.org.acropole.cnab.infra.ProcessadorRetornoAbstract;
import br.org.acropole.cnab.infra.arquivo.CnabCabecalhoInterface;
import br.org.acropole.cnab.infra.arquivo.CnabDetalhamentoInterface;
import br.org.acropole.cnab.infra.arquivo.CnabInterface;
import br.org.acropole.cnab.infra.arquivo.CnabTrailerInterface;
import br.org.acropole.cnab.infra.lote.LoteInterface;
import br.org.acropole.cnab.infra.retorno.RetornoInterface;

import static br.org.acropole.cnab.domain.cnab150.arquivo.Cnab150RegistroTipo.getRegistroTipo;

public class Cnab150ProcessadorRetorno extends ProcessadorRetornoAbstract {

    @Override
    protected boolean processarRetorno(RetornoInterface retorno, CnabInterface composicao) {
        Cnab150RegistroTipo registroTipo = getRegistroTipo(composicao.getRegistro().getCodigo());
        switch (registroTipo) {
            case CABECALHO:
                retorno.setCabecalho(CnabCabecalhoInterface.class.cast(composicao));
                return true;

            case TRAILER:
                retorno.setTrailer(CnabTrailerInterface.class.cast(composicao));
                return true;

            default:
                return false;
        }
    }

    @Override
    protected void processarLote(LoteInterface lote, CnabInterface composicao) {
        Cnab150RegistroTipo registroTipo = getRegistroTipo(composicao.getRegistro().getCodigo());
        switch (registroTipo) {
            case DETALHAMENTO:
                lote.adicionarDetalhamento(CnabDetalhamentoInterface.class.cast(composicao));
                break;
        }
    }

}
