package br.org.acropole.cnab.domain.cnab150.arquivo;

import br.org.acropole.cnab.domain.cnab150.modelo.Movimentacao;
import br.org.acropole.cnab.infra.arquivo.CnabDetalhamentoInterface;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Cnab150Detalhamento extends Cnab150Abstract implements CnabDetalhamentoInterface {

    @Getter
    @Setter
    protected Movimentacao movimentacao;

    public Cnab150Detalhamento(Cnab150DetalhamentoBuilder builder) {
        super(builder);
        this.movimentacao = builder.movimentacao;
    }

    public static class Cnab150DetalhamentoBuilder extends Cnab150BuilderAbstract<Cnab150DetalhamentoBuilder, Cnab150Detalhamento> {

        protected Movimentacao movimentacao;

        public Cnab150DetalhamentoBuilder movimentacao(Movimentacao movimentacao) {
            this.movimentacao = movimentacao;
            return getThis();
        }

        @Override
        public Cnab150DetalhamentoBuilder getThis() {
            return this;
        }

        @Override
        public Cnab150Detalhamento build() {
            return new Cnab150Detalhamento(this);
        }

    }

}
