package br.org.acropole.cnab.domain.modelo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;

@Builder
@ToString
@EqualsAndHashCode
public class Inscricao {

    @Getter
    @Setter
    private String numero;

    @Getter
    @Setter
    private String tipo;

}
