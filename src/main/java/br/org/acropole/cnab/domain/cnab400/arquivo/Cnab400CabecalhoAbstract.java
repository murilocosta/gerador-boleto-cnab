package br.org.acropole.cnab.domain.cnab400.arquivo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public abstract class Cnab400CabecalhoAbstract extends Cnab400Abstract implements Cnab400CabecalhoInterface {

    @Getter
    @Setter
    protected String tipoOperacao;

    @Getter
    @Setter
    protected String identificadorTipoOperacao;

    @Getter
    @Setter
    protected String identificadorTipoServico;

    @Getter
    @Setter
    protected String tipoServico;

    @Getter
    @Setter
    protected Date dataGravacao;

    @Getter
    @Setter
    protected String sequencialRegistro;

    @Getter
    @Setter
    protected Collection<String> complementos;

    @Override
    public void adicionarComplemento(String complemento) {
        this.complementos = (this.complementos == null) ? new HashSet<String>() : this.complementos;
        this.complementos.add(complemento);
    }

    @Override
    public void excluirComplemento(String complemento) {
        this.complementos = (this.complementos == null) ? new HashSet<String>() : this.complementos;
        if (this.complementos.contains(complemento)) {
            this.complementos.add(complemento);
        }
    }

    public <B extends Cnab400CabecalhoBuilderAbstract<B, E>, E extends Cnab400CabecalhoAbstract> Cnab400CabecalhoAbstract(B builder) {
        super(builder);
        this.tipoOperacao = builder.tipoOperacao;
        this.identificadorTipoOperacao = builder.identificadorTipoOperacao;
        this.identificadorTipoServico = builder.identificadorTipoServico;
        this.tipoServico = builder.tipoServico;
        this.dataGravacao = builder.dataGravacao;
        this.sequencialRegistro = builder.sequencialRegistro;
        this.complementos = builder.complementos;
    }

    public abstract static class Cnab400CabecalhoBuilderAbstract<B extends Cnab400CabecalhoBuilderAbstract<B, O>, O extends Cnab400CabecalhoAbstract>
            extends Cnab400BuilderAbstract<B, O> {

        protected String tipoOperacao;
        protected String identificadorTipoOperacao;
        protected String identificadorTipoServico;
        protected String tipoServico;
        protected Date dataGravacao;
        protected String sequencialRegistro;
        protected Collection<String> complementos = new HashSet<String>();

        public B tipoOperacao(String tipoOperacao) {
            this.tipoOperacao = tipoOperacao;
            return getThis();
        }

        public B identificadorTipoOperacao(String identificadorTipoOperacao) {
            this.identificadorTipoOperacao = identificadorTipoOperacao;
            return getThis();
        }

        public B identificadorTipoServico(String identificadorTipoServico) {
            this.identificadorTipoServico = identificadorTipoServico;
            return getThis();
        }

        public B tipoServico(String tipoServico) {
            this.tipoServico = tipoServico;
            return getThis();
        }

        public B dataGravacao(Date dataGravacao) {
            this.dataGravacao = dataGravacao;
            return getThis();
        }

        public B sequencialRegistro(String sequencialRegistro) {
            this.sequencialRegistro = sequencialRegistro;
            return getThis();
        }

        public B complementos(Collection<String> complementos) {
            this.complementos = complementos;
            return getThis();
        }

        public B complemento(String complemento) {
            this.complementos.add(complemento);
            return getThis();
        }

    }

}
