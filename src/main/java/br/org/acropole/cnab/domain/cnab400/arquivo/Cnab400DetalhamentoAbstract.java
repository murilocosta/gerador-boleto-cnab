package br.org.acropole.cnab.domain.cnab400.arquivo;

import br.org.acropole.cnab.domain.modelo.Banco;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public abstract class Cnab400DetalhamentoAbstract extends Cnab400Abstract implements Cnab400DetalhamentoInterface {

    @Getter
    @Setter
    protected BigDecimal taxaDesconto;

    @Getter
    @Setter
    protected BigDecimal taxaIof;

    @Getter
    @Setter
    protected String carteira;

    @Getter
    @Setter
    protected String comando;

    @Getter
    @Setter
    protected Date dataOcorrencia;

    @Getter
    @Setter
    protected String numeroTitulo;

    @Getter
    @Setter
    protected Date dataVencimento;

    @Getter
    @Setter
    protected BigDecimal valor;

    @Getter
    @Setter
    protected String especie;

    @Getter
    @Setter
    protected Date dataCredito;

    @Getter
    @Setter
    protected BigDecimal valorTarifa;

    @Getter
    @Setter
    protected BigDecimal outrasDespesas;

    @Getter
    @Setter
    protected BigDecimal jurosDesconto;

    @Getter
    @Setter
    protected BigDecimal descontoIof;

    @Getter
    @Setter
    protected BigDecimal valorAbatimento;

    @Getter
    @Setter
    protected BigDecimal descontoConcedido;

    @Getter
    @Setter
    protected BigDecimal valorRecebido;

    @Getter
    @Setter
    protected BigDecimal jurosMora;

    @Getter
    @Setter
    protected BigDecimal outrosRecebimentos;

    @Getter
    @Setter
    protected String abatimentoNaoAprovado;

    @Getter
    @Setter
    protected BigDecimal valorLancamento;

    @Getter
    @Setter
    protected String indicativoDc;

    @Getter
    @Setter
    protected String indicadorValor;

    @Getter
    @Setter
    protected BigDecimal valorAjuste;

    @Getter
    @Setter
    protected String canalPagamentoTitulo;

    @Getter
    @Setter
    protected String sequencial;

    @Getter
    @Setter
    protected Date dataEntradaLiquidacao;

    @Getter
    @Setter
    protected Banco bancoEmissor;

    @Getter
    @Setter
    protected Banco bancoRecebedor;

    @Getter
    @Setter
    protected Collection<String> zeros;

    @Getter
    @Setter
    protected Collection<String> brancos;

    public <B extends Cnab400DetalhamentoBuilderAbstract<B, E>, E extends Cnab400DetalhamentoAbstract> Cnab400DetalhamentoAbstract(B builder) {
        super(builder);
        this.taxaDesconto = builder.taxaDesconto;
        this.taxaIof = builder.taxaIof;
        this.carteira = builder.carteira;
        this.comando = builder.comando;
        this.dataOcorrencia = builder.dataOcorrencia;
        this.numeroTitulo = builder.numeroTitulo;
        this.dataVencimento = builder.dataVencimento;
        this.valor = builder.valor;
        this.especie = builder.especie;
        this.dataCredito = builder.dataCredito;
        this.valorTarifa = builder.valorTarifa;
        this.outrasDespesas = builder.outrasDespesas;
        this.jurosDesconto = builder.jurosDesconto;
        this.descontoIof = builder.descontoIof;
        this.valorAbatimento = builder.valorAbatimento;
        this.descontoConcedido = builder.descontoConcedido;
        this.valorRecebido = builder.valorRecebido;
        this.jurosMora = builder.jurosMora;
        this.outrosRecebimentos = builder.outrosRecebimentos;
        this.abatimentoNaoAprovado = builder.abatimentoNaoAprovado;
        this.valorLancamento = builder.valorLancamento;
        this.indicativoDc = builder.indicativoDc;
        this.indicadorValor = builder.indicadorValor;
        this.valorAjuste = builder.valorAjuste;
        this.canalPagamentoTitulo = builder.canalPagamentoTitulo;
        this.sequencial = builder.sequencial;
        this.dataEntradaLiquidacao = builder.dataEntradaLiquidacao;
        this.bancoEmissor = builder.bancoEmissor;
        this.bancoRecebedor = builder.bancoRecebedor;
        this.zeros = builder.zeros;
        this.brancos = builder.brancos;
    }

    public abstract static class Cnab400DetalhamentoBuilderAbstract<B extends Cnab400DetalhamentoBuilderAbstract<B, O>, O extends Cnab400DetalhamentoAbstract>
            extends Cnab400BuilderAbstract<B, O> {

        protected BigDecimal taxaDesconto;
        protected BigDecimal taxaIof;
        protected String carteira;
        protected String comando;
        protected Date dataOcorrencia;
        protected String numeroTitulo;
        protected Date dataVencimento;
        protected BigDecimal valor;
        protected String especie;
        protected Date dataCredito;
        protected BigDecimal valorTarifa;
        protected BigDecimal outrasDespesas;
        protected BigDecimal jurosDesconto;
        protected BigDecimal descontoIof;
        protected BigDecimal valorAbatimento;
        protected BigDecimal descontoConcedido;
        protected BigDecimal valorRecebido;
        protected BigDecimal jurosMora;
        protected BigDecimal outrosRecebimentos;
        protected String abatimentoNaoAprovado;
        protected BigDecimal valorLancamento;
        protected String indicativoDc;
        protected String indicadorValor;
        protected BigDecimal valorAjuste;
        protected String canalPagamentoTitulo;
        protected String sequencial;
        protected Date dataEntradaLiquidacao;
        protected Banco bancoEmissor;
        protected Banco bancoRecebedor;
        protected Collection<String> zeros;
        protected Collection<String> brancos;

        public B taxaDesconto(BigDecimal taxaDesconto) {
            this.taxaDesconto = taxaDesconto;
            return getThis();
        }

        public B taxaIof(BigDecimal taxaIof) {
            this.taxaIof = taxaIof;
            return getThis();
        }

        public B carteira(String carteira) {
            this.carteira = carteira;
            return getThis();
        }

        public B comando(String comando) {
            this.comando = comando;
            return getThis();
        }

        public B dataOcorrencia(Date dataOcorrencia) {
            this.dataOcorrencia = dataOcorrencia;
            return getThis();
        }

        public B numeroTitulo(String numeroTitulo) {
            this.numeroTitulo = numeroTitulo;
            return getThis();
        }

        public B dataVencimento(Date dataVencimento) {
            this.dataVencimento = dataVencimento;
            return getThis();
        }

        public B valor(BigDecimal valor) {
            this.valor = valor;
            return getThis();
        }

        public B especie(String especie) {
            this.especie = especie;
            return getThis();
        }

        public B dataCredito(Date dataCredito) {
            this.dataCredito = dataCredito;
            return getThis();
        }

        public B valorTarifa(BigDecimal valorTarifa) {
            this.valorTarifa = valorTarifa;
            return getThis();
        }

        public B outrasDespesas(BigDecimal outrasDespesas) {
            this.outrasDespesas = outrasDespesas;
            return getThis();
        }

        public B jurosDesconto(BigDecimal jurosDesconto) {
            this.jurosDesconto = jurosDesconto;
            return getThis();
        }

        public B descontoIof(BigDecimal descontoIof) {
            this.descontoIof = descontoIof;
            return getThis();
        }

        public B valorAbatimento(BigDecimal valorAbatimento) {
            this.valorAbatimento = valorAbatimento;
            return getThis();
        }

        public B descontoConcedido(BigDecimal descontoConcedido) {
            this.descontoConcedido = descontoConcedido;
            return getThis();
        }

        public B valorRecebido(BigDecimal valorRecebido) {
            this.valorRecebido = valorRecebido;
            return getThis();
        }

        public B jurosMora(BigDecimal jurosMora) {
            this.jurosMora = jurosMora;
            return getThis();
        }

        public B outrosRecebimentos(BigDecimal outrosRecebimentos) {
            this.outrosRecebimentos = outrosRecebimentos;
            return getThis();
        }

        public B abatimentoNaoAprovado(String abatimentoNaoAprovado) {
            this.abatimentoNaoAprovado = abatimentoNaoAprovado;
            return getThis();
        }

        public B valorLancamento(BigDecimal valorLancamento) {
            this.valorLancamento = valorLancamento;
            return getThis();
        }

        public B indicativoDc(String indicativoDc) {
            this.indicativoDc = indicativoDc;
            return getThis();
        }

        public B indicadorValor(String indicadorValor) {
            this.indicadorValor = indicadorValor;
            return getThis();
        }

        public B valorAjuste(BigDecimal valorAjuste) {
            this.valorAjuste = valorAjuste;
            return getThis();
        }

        public B canalPagamentoTitulo(String canalPagamentoTitulo) {
            this.canalPagamentoTitulo = canalPagamentoTitulo;
            return getThis();
        }

        public B sequencial(String sequencial) {
            this.sequencial = sequencial;
            return getThis();
        }

        public B dataEntradaLiquidacao(Date dataEntradaLiquidacao) {
            this.dataEntradaLiquidacao = dataEntradaLiquidacao;
            return getThis();
        }

        public B bancoEmissor(Banco bancoEmissor) {
            this.bancoEmissor = bancoEmissor;
            return getThis();
        }

        public B bancoRecebedor(Banco bancoRecebedor) {
            this.bancoRecebedor = bancoRecebedor;
            return getThis();
        }

        public B zeros(Collection<String> zeros) {
            this.zeros = zeros;
            return getThis();
        }

        public B zero(String zero) {
            this.zeros.add(zero);
            return getThis();
        }

        public B brancos(Collection<String> brancos) {
            this.brancos = brancos;
            return getThis();
        }
    }

}
