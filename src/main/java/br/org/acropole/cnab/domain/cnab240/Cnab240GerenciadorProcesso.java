package br.org.acropole.cnab.domain.cnab240;

import br.org.acropole.cnab.domain.GerenciadorProcessoAbstract;
import br.org.acropole.cnab.domain.cnab240.processador.Cnab240Processador;
import br.org.acropole.cnab.domain.cnab240.processador.Cnab240ProcessadorRetorno;

public class Cnab240GerenciadorProcesso extends GerenciadorProcessoAbstract<Cnab240Processador, Cnab240ProcessadorRetorno> {

    public Cnab240GerenciadorProcesso(Cnab240Processador processadorArquivo, Cnab240ProcessadorRetorno processadorRetorno) {
        super(processadorArquivo, processadorRetorno);
    }

}
