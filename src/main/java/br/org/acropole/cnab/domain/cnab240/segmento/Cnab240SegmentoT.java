package br.org.acropole.cnab.domain.cnab240.segmento;

import br.org.acropole.cnab.domain.cnab240.arquivo.Cnab240Detalhamento;
import br.org.acropole.cnab.domain.cnab240.arquivo.Cnab240Detalhamento.Cnab240DetalhamentoBuilder;
import br.org.acropole.cnab.domain.modelo.*;

import static br.org.acropole.cnab.infra.util.CollectionUtils.criarColecaoComElemento;

public class Cnab240SegmentoT extends Cnab240SegmentoAbstract {

    @Override
    public Cnab240Detalhamento montarDetalhamento(String linha) {
        return new Cnab240DetalhamentoBuilder()
                .cedente(Cedente.builder()
                        .banco(Banco.builder()
                                .codigo(getString(linha, 1, 3))
                                .agencia(getString(linha, 18, 5))
                                .digitoVerificadorAgencia(getString(linha, 23, 1))
                                .conta(getString(linha, 24, 12))
                                .digitoVerificadorConta(getString(linha, 36, 1))
                                .build())
                        .build())
                .lote(getString(linha, 4, 4))
                .registro(getRegistro(linha, 8, 1))
                .numeroRegistroLote(getString(linha, 9, 5))
                .segmento(getSegmento(linha, 14, 1))
                .tipoMovimentacao(getString(linha, 15, 1))
                .codigoMovimentacao(getString(linha, 16, 2))
                .nossoNumero(getString(linha, 38, 20))
                .carteira(getString(linha, 58, 1))
                .numeroDocumento(getString(linha, 59, 15))
                .dataVencimento(getDate(linha, 74, 8))
                .valorTitulo(getDecimal(linha, 82, 15))
                .empresa(Empresa.builder()
                        .usos(criarColecaoComElemento(getString(linha, 106, 25)))
                        .build())
                .codigoMoeda(getString(linha, 131, 2))
                .sacado(Sacado.builder()
                        .banco(Banco.builder()
                                .codigo(getString(linha, 97, 3))
                                .agencia(getString(linha, 100, 5))
                                .digitoVerificadorAgencia(getString(linha, 105, 1))
                                .build())
                        .inscricao(Inscricao.builder()
                                .tipo(getString(linha, 133, 1))
                                .numero(getString(linha, 134, 15))
                                .build())
                        .nome(getString(linha, 149, 40))
                        .build())
                .numeroContrato(getString(linha, 189, 10))
                .valorTarifa(getDecimal(linha, 199, 15))
                .ocorrencia(getString(linha, 214, 10))
                .cnab(getString(linha, 224, 17))
                .build();
    }

}
