package br.org.acropole.cnab.domain.cnab150.arquivo;

import br.org.acropole.cnab.infra.arquivo.CnabTrailerInterface;
import lombok.*;

import java.math.BigDecimal;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Cnab150Trailer extends Cnab150Abstract implements CnabTrailerInterface {

    @Getter
    @Setter
    protected Integer quantidadeRegistros;

    @Getter
    @Setter
    protected BigDecimal valorTotal;

    public Cnab150Trailer(Cnab150TrailerBuilder builder) {
        super(builder);
        this.quantidadeRegistros = builder.quantidadeRegistros;
        this.valorTotal = builder.valorTotal;
    }

    public static class Cnab150TrailerBuilder extends Cnab150BuilderAbstract<Cnab150TrailerBuilder, Cnab150Trailer> {

        protected Integer quantidadeRegistros;
        protected BigDecimal valorTotal;

        public Cnab150TrailerBuilder quantidadeRegistros(Integer quantidadeRegistros) {
            this.quantidadeRegistros = quantidadeRegistros;
            return getThis();
        }

        public Cnab150TrailerBuilder valorTotal(BigDecimal valorTotal) {
            this.valorTotal = valorTotal;
            return getThis();
        }

        @Override
        public Cnab150TrailerBuilder getThis() {
            return this;
        }

        @Override
        public Cnab150Trailer build() {
            return new Cnab150Trailer(this);
        }
    }

}
