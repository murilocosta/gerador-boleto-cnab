package br.org.acropole.cnab.domain.cnab400.arquivo;

import br.org.acropole.cnab.infra.arquivo.CnabCabecalhoInterface;

import java.util.Collection;
import java.util.Date;

public interface Cnab400CabecalhoInterface extends CnabCabecalhoInterface {

    String getTipoOperacao();

    void setTipoOperacao(String tipoOperacao);

    String getIdentificadorTipoOperacao();

    void setIdentificadorTipoOperacao(String identificadorTipoOperacao);

    String getIdentificadorTipoServico();

    void setIdentificadorTipoServico(String identificadorTipoServico);

    String getTipoServico();

    void setTipoServico(String tipoServico);

    Date getDataGravacao();

    void setDataGravacao(Date dataGravacao);

    String getSequencialRegistro();

    void setSequencialRegistro(String sequencialReg);

    Collection<String> getComplementos();

    void setComplementos(Collection<String> complementos);

    void adicionarComplemento(String complemento);

    void excluirComplemento(String complemento);

}
