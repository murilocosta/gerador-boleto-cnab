package br.org.acropole.cnab.domain.modelo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Sacado extends Pessoa {

    @Getter
    @Setter
    private Inscricao inscricao;

    @Getter
    @Setter
    private Ocorrencia ocorrencia;

    @Builder
    public Sacado(String nome, Banco banco, Inscricao inscricao, Ocorrencia ocorrencia) {
        super(nome, banco);
        this.inscricao = inscricao;
        this.ocorrencia = ocorrencia;
    }
}
