package br.org.acropole.cnab.domain.modelo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;

import java.util.Collection;

@Builder
@ToString
@EqualsAndHashCode
public class Banco {

    @Getter
    @Setter
    private String codigo;

    @Getter
    @Setter
    private String nome;

    @Getter
    @Setter
    private String agencia;

    @Getter
    @Setter
    private String digitoVerificadorAgencia;

    @Getter
    @Setter
    private String conta;

    @Getter
    @Setter
    private String digitoVerificadorConta;

    @Getter
    @Setter
    private String digitoVerificadorAgenciaConta;

    @Getter
    @Setter
    private Collection<String> reservados;

}
