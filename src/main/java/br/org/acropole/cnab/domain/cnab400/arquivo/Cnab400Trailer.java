package br.org.acropole.cnab.domain.cnab400.arquivo;

import br.org.acropole.cnab.domain.modelo.Banco;
import br.org.acropole.cnab.domain.modelo.Cobranca;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;
import java.util.HashSet;

public class Cnab400Trailer extends Cnab400Abstract implements Cnab400TrailerInterface {

    @Getter
    @Setter
    protected String retorno;

    @Getter
    @Setter
    protected String tipoRegistro;

    @Getter
    @Setter
    protected String sequencial;

    @Getter
    @Setter
    protected Banco banco;

    @Getter
    @Setter
    protected Cobranca simples;

    @Getter
    @Setter
    protected Cobranca vinculada;

    @Getter
    @Setter
    protected Cobranca caucionada;

    @Getter
    @Setter
    protected Cobranca vendor;

    @Getter
    @Setter
    protected Cobranca descontada;

    @Getter
    @Setter
    protected Collection<String> brancos;

    public Cnab400Trailer(Cnab400TrailerBuilder builder) {
        super(builder);
        this.retorno = builder.retorno;
        this.tipoRegistro = builder.tipoRegistro;
        this.sequencial = builder.sequencial;
        this.banco = builder.banco;
        this.simples = builder.simples;
        this.vinculada = builder.vinculada;
        this.caucionada = builder.caucionada;
        this.vendor = builder.vendor;
        this.descontada = builder.descontada;
        this.brancos = builder.brancos;
    }

    public static class Cnab400TrailerBuilder extends Cnab400BuilderAbstract<Cnab400TrailerBuilder, Cnab400Trailer> {

        protected String retorno;
        protected String tipoRegistro;
        protected String sequencial;
        protected Banco banco;
        protected Cobranca simples;
        protected Cobranca vinculada;
        protected Cobranca caucionada;
        protected Cobranca vendor;
        protected Cobranca descontada;
        protected Collection<String> brancos = new HashSet<String>();

        public Cnab400TrailerBuilder retorno(String retorno) {
            this.retorno = retorno;
            return getThis();
        }

        public Cnab400TrailerBuilder tipoRegistro(String tipoRegistro) {
            this.tipoRegistro = tipoRegistro;
            return getThis();
        }

        public Cnab400TrailerBuilder sequencial(String sequencial) {
            this.sequencial = sequencial;
            return getThis();
        }

        public Cnab400TrailerBuilder banco(Banco banco) {
            this.banco = banco;
            return getThis();
        }

        public Cnab400TrailerBuilder simples(Cobranca simples) {
            this.simples = simples;
            return getThis();
        }

        public Cnab400TrailerBuilder vinculada(Cobranca vinculada) {
            this.vinculada = vinculada;
            return getThis();
        }

        public Cnab400TrailerBuilder caucionada(Cobranca caucionada) {
            this.caucionada = caucionada;
            return getThis();
        }

        public Cnab400TrailerBuilder vendor(Cobranca vendor) {
            this.vendor = vendor;
            return getThis();
        }

        public Cnab400TrailerBuilder descontada(Cobranca descontada) {
            this.descontada = descontada;
            return getThis();
        }

        public Cnab400TrailerBuilder brancos(Collection<String> brancos) {
            this.brancos = brancos;
            return getThis();
        }

        public Cnab400TrailerBuilder branco(String branco) {
            this.brancos.add(branco);
            return getThis();
        }

        @Override
        public Cnab400TrailerBuilder getThis() {
            return this;
        }

        @Override
        public Cnab400Trailer build() {
            return new Cnab400Trailer(this);
        }

    }

}
