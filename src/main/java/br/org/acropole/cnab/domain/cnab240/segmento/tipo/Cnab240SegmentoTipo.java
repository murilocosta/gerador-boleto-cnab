package br.org.acropole.cnab.domain.cnab240.segmento.tipo;

import br.org.acropole.cnab.infra.arquivo.CnabRegistroTipo;

public enum Cnab240SegmentoTipo implements CnabRegistroTipo {

    SEGMENTO_J("J"), SEGMENTO_T("T"), SEGMENTO_U("U");

    private String codigo;

    private Cnab240SegmentoTipo(String codigo) {
        this.codigo = codigo;
    }

    @Override
    public String getCodigo() {
        return codigo;
    }

    public static Cnab240SegmentoTipo getSegmentoTipo(String tipo) {
        for (Cnab240SegmentoTipo segmentoTipo : values()) {
            if (segmentoTipo.getCodigo().equals(tipo)) {
                return segmentoTipo;
            }
        }
        return null;
    }
}
