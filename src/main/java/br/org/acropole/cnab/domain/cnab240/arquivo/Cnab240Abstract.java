package br.org.acropole.cnab.domain.cnab240.arquivo;

import br.org.acropole.cnab.infra.arquivo.CnabAbstract;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public abstract class Cnab240Abstract extends CnabAbstract {

    public <B extends Cnab240BuilderAbstract<B, E>, E extends Cnab240Abstract> Cnab240Abstract(B builder) {
        super(builder);
    }

    public abstract static class Cnab240BuilderAbstract<B extends Cnab240BuilderAbstract<B, E>, E extends Cnab240Abstract>
            extends CnabBuilderAbstract<B, E> {

    }

}
