package br.org.acropole.cnab.domain.cnab400.arquivo;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Cnab400Detalhamento extends Cnab400DetalhamentoAbstract {

    public Cnab400Detalhamento(Cnab400DetalhamentoBuilder builder) {
        super(builder);
    }

    public static class Cnab400DetalhamentoBuilder extends Cnab400DetalhamentoBuilderAbstract<Cnab400DetalhamentoBuilder, Cnab400Detalhamento> {

        @Override
        public Cnab400DetalhamentoBuilder getThis() {
            return this;
        }

        @Override
        public Cnab400Detalhamento build() {
            return new Cnab400Detalhamento(this);
        }

    }

}
