package br.org.acropole.cnab.domain.cnab240.arquivo;

import br.org.acropole.cnab.domain.cnab240.modelo.Titulo;
import br.org.acropole.cnab.domain.modelo.Ocorrencia;
import br.org.acropole.cnab.infra.arquivo.CnabDetalhamentoInterface;
import br.org.acropole.cnab.infra.arquivo.CnabRegistroTipo;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Cnab240Detalhamento extends Cnab240Abstract implements CnabDetalhamentoInterface {

    @Getter
    @Setter
    protected String numeroRegistroLote;

    @Getter
    @Setter
    protected CnabRegistroTipo segmento;

    @Getter
    @Setter
    protected String tipoMovimentacao;

    @Getter
    @Setter
    protected String codigoMovimentacao;

    @Getter
    @Setter
    protected String codigoBarras;

    @Getter
    @Setter
    protected String carteira;

    @Getter
    @Setter
    protected String numeroDocumento;

    @Getter
    @Setter
    protected String numeroContrato;

    @Getter
    @Setter
    protected BigDecimal valorTarifa;

    @Getter
    @Setter
    protected Date dataVencimento;

    @Getter
    @Setter
    protected Date dataPagamento;

    @Getter
    @Setter
    protected BigDecimal valorTitulo;

    @Getter
    @Setter
    protected BigDecimal valorPagamento;

    @Getter
    @Setter
    protected BigDecimal valorDesconto;

    @Getter
    @Setter
    protected BigDecimal valorAcrescimos;

    @Getter
    @Setter
    protected BigDecimal quantidadeMoeda;

    @Getter
    @Setter
    protected String referenciaSacado;

    @Getter
    @Setter
    protected String nossoNumero;

    @Getter
    @Setter
    protected String codigoMoeda;

    @Getter
    @Setter
    protected BigDecimal outrasDespesas;

    @Getter
    @Setter
    protected BigDecimal outrosCreditos;

    @Getter
    @Setter
    protected Date dataOcorrencia;

    @Getter
    @Setter
    protected Date dataCredito;

    @Getter
    @Setter
    protected Titulo titulo;

    @Getter
    @Setter
    protected String numeroSequencial;

    @Getter
    @Setter
    protected String numeroAutenticacao;

    @Getter
    @Setter
    protected BigDecimal valorRecebido;

    @Getter
    @Setter
    protected Ocorrencia ocorrencia;

    public Cnab240Detalhamento(Cnab240DetalhamentoBuilder builder) {
        super(builder);
        this.numeroRegistroLote = builder.numeroRegistroLote;
        this.segmento = builder.segmento;
        this.tipoMovimentacao = builder.tipoMovimentacao;
        this.codigoMovimentacao = builder.codigoMovimentacao;
        this.codigoBarras = builder.codigoBarras;
        this.carteira = builder.carteira;
        this.numeroDocumento = builder.numeroDocumento;
        this.numeroContrato = builder.numeroContrato;
        this.valorTarifa = builder.valorTarifa;
        this.dataVencimento = builder.dataVencimento;
        this.dataPagamento = builder.dataPagamento;
        this.valorTitulo = builder.valorTitulo;
        this.valorPagamento = builder.valorPagamento;
        this.valorDesconto = builder.valorDesconto;
        this.valorAcrescimos = builder.valorAcrescimos;
        this.quantidadeMoeda = builder.quantidadeMoeda;
        this.referenciaSacado = builder.referenciaSacado;
        this.nossoNumero = builder.nossoNumero;
        this.codigoMoeda = builder.codigoMoeda;
        this.outrasDespesas = builder.outrasDespesas;
        this.outrosCreditos = builder.outrosCreditos;
        this.dataOcorrencia = builder.dataOcorrencia;
        this.dataCredito = builder.dataCredito;
        this.titulo = builder.titulo;
        this.numeroSequencial = builder.numeroSequencial;
        this.numeroAutenticacao = builder.numeroAutenticacao;
        this.valorRecebido = builder.valorRecebido;
        this.ocorrencia = builder.ocorrencia;
    }

    public static class Cnab240DetalhamentoBuilder extends Cnab240BuilderAbstract<Cnab240DetalhamentoBuilder, Cnab240Detalhamento> {

        protected String numeroRegistroLote;
        protected CnabRegistroTipo segmento;
        protected String tipoMovimentacao;
        protected String codigoMovimentacao;
        protected String codigoBarras;
        protected String carteira;
        protected String numeroDocumento;
        protected String numeroContrato;
        protected BigDecimal valorTarifa;
        protected Date dataVencimento;
        protected Date dataPagamento;
        protected BigDecimal valorTitulo;
        protected BigDecimal valorPagamento;
        protected BigDecimal valorDesconto;
        protected BigDecimal valorAcrescimos;
        protected BigDecimal quantidadeMoeda;
        protected String referenciaSacado;
        protected String nossoNumero;
        protected String codigoMoeda;
        protected BigDecimal outrasDespesas;
        protected BigDecimal outrosCreditos;
        protected Date dataOcorrencia;
        protected Date dataCredito;
        protected Titulo titulo;
        protected String numeroSequencial;
        protected String numeroAutenticacao;
        protected BigDecimal valorRecebido;
        protected Ocorrencia ocorrencia;

        public Cnab240DetalhamentoBuilder numeroRegistroLote(String numeroRegistroLote) {
            this.numeroRegistroLote = numeroRegistroLote;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder segmento(CnabRegistroTipo segmento) {
            this.segmento = segmento;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder tipoMovimentacao(String tipoMovimentacao) {
            this.tipoMovimentacao = tipoMovimentacao;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder codigoMovimentacao(String codigoMovimentacao) {
            this.codigoMovimentacao = codigoMovimentacao;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder codigoBarras(String codigoBarras) {
            this.codigoBarras = codigoBarras;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder carteira(String carteira) {
            this.carteira = carteira;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder numeroDocumento(String numeroDocumento) {
            this.numeroDocumento = numeroDocumento;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder numeroContrato(String numeroContrato) {
            this.numeroContrato = numeroContrato;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder valorTarifa(BigDecimal valorTarifa) {
            this.valorTarifa = valorTarifa;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder dataVencimento(Date dataVencimento) {
            this.dataVencimento = dataVencimento;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder dataPagamento(Date dataPagamento) {
            this.dataPagamento = dataPagamento;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder valorTitulo(BigDecimal valorTitulo) {
            this.valorTitulo = valorTitulo;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder valorPagamento(BigDecimal valorPagamento) {
            this.valorPagamento = valorPagamento;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder valorDesconto(BigDecimal valorDesconto) {
            this.valorDesconto = valorDesconto;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder valorAcrescimos(BigDecimal valorAcrescimos) {
            this.valorAcrescimos = valorAcrescimos;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder quantidadeMoeda(BigDecimal quantidadeMoeda) {
            this.quantidadeMoeda = quantidadeMoeda;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder referenciaSacado(String referenciaSacado) {
            this.referenciaSacado = referenciaSacado;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder nossoNumero(String nossoNumero) {
            this.nossoNumero = nossoNumero;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder codigoMoeda(String codigoMoeda) {
            this.codigoMoeda = codigoMoeda;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder outrasDespesas(BigDecimal outrasDespesas) {
            this.outrasDespesas = outrasDespesas;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder outrosCreditos(BigDecimal outrosCreditos) {
            this.outrosCreditos = outrosCreditos;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder dataOcorrencia(Date dataOcorrencia) {
            this.dataOcorrencia = dataOcorrencia;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder dataCredito(Date dataCredito) {
            this.dataCredito = dataCredito;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder titulo(Titulo titulo) {
            this.titulo = titulo;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder numeroSequencial(String numeroSequencial) {
            this.numeroSequencial = numeroSequencial;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder numeroAutenticacao(String numeroAutenticacao) {
            this.numeroAutenticacao = numeroAutenticacao;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder valorRecebido(BigDecimal valorRecebido) {
            this.valorRecebido = valorRecebido;
            return getThis();
        }

        public Cnab240DetalhamentoBuilder ocorrencia(Ocorrencia ocorrencia) {
            this.ocorrencia = ocorrencia;
            return getThis();
        }

        @Override
        public Cnab240DetalhamentoBuilder getThis() {
            return this;
        }

        @Override
        public Cnab240Detalhamento build() {
            return new Cnab240Detalhamento(this);
        }

    }

}
