package br.org.acropole.cnab.domain.cnab400.convenio;

import br.org.acropole.cnab.domain.cnab400.arquivo.Cnab400CabecalhoAbstract;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Cnab400CabecalhoConvenio extends Cnab400CabecalhoAbstract implements Cnab400CabecalhoConvenioInterface {

    @Getter
    @Setter
    protected String convenio;

    @Getter
    @Setter
    protected String sequencialRetorno;

    public Cnab400CabecalhoConvenio(Cnab400CabecalhoConvenioBuilder builder) {
        super(builder);
        this.convenio = builder.convenio;
        this.sequencialRetorno = builder.sequencialRetorno;
    }

    public static class Cnab400CabecalhoConvenioBuilder
            extends Cnab400CabecalhoBuilderAbstract<Cnab400CabecalhoConvenioBuilder, Cnab400CabecalhoConvenio> {

        protected String convenio;
        protected String sequencialRetorno;

        public Cnab400CabecalhoConvenioBuilder convenio(String convenio) {
            this.convenio = convenio;
            return getThis();
        }

        public Cnab400CabecalhoConvenioBuilder sequencialRetorno(String sequencialRetorno) {
            this.sequencialRetorno = sequencialRetorno;
            return getThis();
        }

        @Override
        public Cnab400CabecalhoConvenioBuilder getThis() {
            return this;
        }

        @Override
        public Cnab400CabecalhoConvenio build() {
            return new Cnab400CabecalhoConvenio(this);
        }

    }

}
