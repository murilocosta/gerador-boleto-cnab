package br.org.acropole.cnab.domain.cnab240.arquivo;

import br.org.acropole.cnab.infra.arquivo.CnabRegistroTipo;

public enum Cnab240RegistroTipo implements CnabRegistroTipo {

    CABECALHO("0"), CABECALHO_LOTE("1"), DETALHAMENTO("3"), TRAILER_LOTE("5"), TRAILER("9");

    private String codigo;

    private Cnab240RegistroTipo(String codigo) {
        this.codigo = codigo;
    }

    @Override
    public String getCodigo() {
        return codigo;
    }

    public static Cnab240RegistroTipo getColunaTipo(String tipo) {
        for (Cnab240RegistroTipo colunaTipo : values()) {
            if (colunaTipo.getCodigo().equals(tipo)) {
                return colunaTipo;
            }
        }
        return null;
    }

}
