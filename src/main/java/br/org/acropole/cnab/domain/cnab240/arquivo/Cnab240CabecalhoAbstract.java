package br.org.acropole.cnab.domain.cnab240.arquivo;

import br.org.acropole.cnab.infra.arquivo.CnabCabecalhoInterface;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public abstract class Cnab240CabecalhoAbstract extends Cnab240Abstract implements CnabCabecalhoInterface {

    @Getter
    @Setter
    protected String convenio;

    @Getter
    @Setter
    protected String versaoLayout;

    public <B extends Cnab240CabecalhoBuilderAbstract<B, E>, E extends Cnab240Abstract> Cnab240CabecalhoAbstract(B builder) {
        super(builder);
        this.convenio = builder.convenio;
        this.versaoLayout = builder.versaoLayout;
    }

    public abstract static class Cnab240CabecalhoBuilderAbstract<B extends Cnab240CabecalhoBuilderAbstract<B, E>, E extends Cnab240Abstract>
            extends Cnab240BuilderAbstract<B, E> {

        protected String convenio;
        protected String versaoLayout;

        public B convenio(String convenio) {
            this.convenio = convenio;
            return getThis();
        }

        public B versaoLayout(String versaoLayout) {
            this.versaoLayout = versaoLayout;
            return getThis();
        }

    }

}
