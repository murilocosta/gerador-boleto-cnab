package br.org.acropole.cnab.domain.cnab400.processador;

import br.org.acropole.cnab.domain.cnab400.arquivo.Cnab400CabecalhoInterface;
import br.org.acropole.cnab.domain.cnab400.arquivo.Cnab400DetalhamentoInterface;
import br.org.acropole.cnab.domain.cnab400.arquivo.Cnab400RegistroTipo;
import br.org.acropole.cnab.domain.cnab400.arquivo.Cnab400TrailerInterface;
import br.org.acropole.cnab.infra.ProcessadorRetornoAbstract;
import br.org.acropole.cnab.infra.arquivo.CnabInterface;
import br.org.acropole.cnab.infra.lote.LoteInterface;
import br.org.acropole.cnab.infra.retorno.RetornoInterface;

public class Cnab400ProcessadorRetorno extends ProcessadorRetornoAbstract {

    @Override
    protected boolean processarRetorno(RetornoInterface retorno, CnabInterface composicao) {
        Cnab400RegistroTipo tipo = Cnab400RegistroTipo.getRegistroTipo(composicao.getRegistro().getCodigo());
        switch (tipo) {
            case CABECALHO:
                retorno.setCabecalho(Cnab400CabecalhoInterface.class.cast(composicao));
                return true;

            case TRAILER:
                retorno.setTrailer(Cnab400TrailerInterface.class.cast(composicao));
                return true;

            default:
                return false;
        }
    }

    @Override
    protected void processarLote(LoteInterface lote, CnabInterface composicao) {
        lote.adicionarDetalhamento(Cnab400DetalhamentoInterface.class.cast(composicao));
    }

}
