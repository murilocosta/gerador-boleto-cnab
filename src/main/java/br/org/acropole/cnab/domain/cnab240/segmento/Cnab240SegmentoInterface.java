package br.org.acropole.cnab.domain.cnab240.segmento;

import br.org.acropole.cnab.domain.cnab240.arquivo.Cnab240Detalhamento;

public interface Cnab240SegmentoInterface {

    Cnab240Detalhamento montarDetalhamento(String linha);

}
