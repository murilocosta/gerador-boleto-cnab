package br.org.acropole.cnab.domain.modelo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;

import java.math.BigDecimal;

@Builder
@ToString
@EqualsAndHashCode
public class Cobranca {

    @Getter
    @Setter
    private String quantidadeTitulos;

    @Getter
    @Setter
    private BigDecimal valorTotal;

    @Getter
    @Setter
    private String numeroAviso;

}
