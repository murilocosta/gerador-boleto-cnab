package br.org.acropole.cnab.domain.cnab150.arquivo;

import br.org.acropole.cnab.infra.arquivo.CnabAbstract;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public abstract class Cnab150Abstract extends CnabAbstract {

    @Getter
    @Setter
    protected String codigoBarras;

    @Getter
    @Setter
    protected String filler;

    public <B extends Cnab150BuilderAbstract<B, E>, E extends Cnab150Abstract> Cnab150Abstract(B builder) {
        super(builder);
        this.codigoBarras = builder.codigoBarras;
        this.filler = builder.filler;
    }

    public abstract static class Cnab150BuilderAbstract<B extends Cnab150BuilderAbstract<B, E>, E extends Cnab150Abstract>
            extends CnabBuilderAbstract<B, E> {

        protected String codigoBarras;
        protected String filler;

        public B codigoBarras(String codigoBarras) {
            this.codigoBarras = codigoBarras;
            return getThis();
        }

        public B filler(String filler) {
            this.filler = filler;
            return getThis();
        }

    }

}
