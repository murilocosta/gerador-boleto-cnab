package br.org.acropole.cnab.domain.cnab150.processador;

import br.org.acropole.cnab.domain.cnab150.arquivo.Cnab150Cabecalho;
import br.org.acropole.cnab.domain.cnab150.arquivo.Cnab150Cabecalho.Cnab150CabecalhoBuilder;
import br.org.acropole.cnab.domain.cnab150.arquivo.Cnab150Detalhamento;
import br.org.acropole.cnab.domain.cnab150.arquivo.Cnab150Detalhamento.Cnab150DetalhamentoBuilder;
import br.org.acropole.cnab.domain.cnab150.arquivo.Cnab150RegistroTipo;
import br.org.acropole.cnab.domain.cnab150.arquivo.Cnab150Trailer;
import br.org.acropole.cnab.domain.cnab150.arquivo.Cnab150Trailer.Cnab150TrailerBuilder;
import br.org.acropole.cnab.domain.modelo.Banco;
import br.org.acropole.cnab.domain.modelo.Cedente;
import br.org.acropole.cnab.domain.modelo.Empresa;
import br.org.acropole.cnab.domain.cnab150.modelo.Movimentacao;
import br.org.acropole.cnab.domain.cnab150.modelo.Pagamento;
import br.org.acropole.cnab.domain.cnab150.modelo.Recebimento;
import br.org.acropole.cnab.infra.ProcessadorArquivoAbstract;
import br.org.acropole.cnab.infra.arquivo.CnabInterface;

import java.io.File;

import static br.org.acropole.cnab.domain.cnab150.arquivo.Cnab150RegistroTipo.getRegistroTipo;
import static br.org.acropole.cnab.domain.cnab150.arquivo.Cnab150RegistroTipo.valueOf;

/**
 * Classe para leitura de arquivos de retorno para cobranças padrão CNAB150.<br/>
 * Layout Padrão Febraban 150 posições
 */
public class Cnab150Processador extends ProcessadorArquivoAbstract {

    public Cnab150Processador(File arquivo) {
        super(arquivo);
    }

    private Cnab150Cabecalho processarCabecalhoArquivo(String linha) {
        return new Cnab150CabecalhoBuilder()
                .registro(getRegistro(linha, 1, 1))
                .remessa(getString(linha, 2, 1))
                .convenio(getString(linha, 3, 20))
                .empresa(Empresa.builder()
                        .nome(getString(linha, 23, 20))
                        .build())
                .cedente(Cedente.builder()
                        .banco(Banco.builder()
                                .codigo(getString(linha, 43, 3))
                                .nome(getString(linha, 46, 20))
                                .build())
                        .build())
                .dataGeracao(getDate(linha, 66, 8))
                .sequencialRetorno(getString(linha, 80, 2))
                .versaoLayout(getString(linha, 82, 17))
                .filler(getString(linha, 99, 52))
                .build();
    }

    private Cnab150Detalhamento processarDetalhamentoArquivo(String linha) {
        Pagamento pagamento = Pagamento.builder()
                .dataPagamento(getDate(linha, 22, 8))
                .formaPagamento(getString(linha, 141, 1))
                .build();
        Recebimento recebimento = Recebimento.builder()
                .dataCredito(getDate(linha, 30, 8))
                .valorRecebido(getDecimal(linha, 82, 12))
                .valorTarifa(getDecimal(linha, 94, 7))
                .numeroSequencial(getString(linha, 101, 8))
                .codigoAgenciaArrecadadora(getString(linha, 109, 8))
                .formaArrecadacao(getString(linha, 117, 1))
                .numeroAutenticacao(getString(linha, 118, 23))
                .build();
        Movimentacao movimentacao = Movimentacao.builder()
                .pagamento(pagamento)
                .recebimento(recebimento)
                .build();
        return new Cnab150DetalhamentoBuilder()
                .registro(getRegistro(linha, 1, 1))
                .cedente(Cedente.builder()
                        .banco(Banco.builder().agencia(getString(linha, 2, 4)).conta(getString(linha, 6, 14))
                                .digitoVerificadorConta(getString(linha, 20, 1)).build())
                        .build())
                .codigoBarras(getString(linha, 38, 44))
                .filler(getString(linha, 142, 9))
                .movimentacao(movimentacao)
                .build();
    }

    private Cnab150Trailer processarTrailerArquivo(String linha) {
        return new Cnab150TrailerBuilder()
                .registro(getRegistro(linha, 1, 1))
                .quantidadeRegistros(getInteger(linha, 2, 6))
                .valorTotal(getDecimal(linha, 8, 17))
                .filler(getString(linha, 25, 126))
                .build();
    }

    @Override
    public CnabInterface processarLinha(String linha) {
        // É necessário adicionar um espaço vazio no início da linha para que possamos trabalhar com
        // índices iniciando com 1 e assim, obter os valores dos campos nas posições exatas definidas
        // pelo manual CNAB150
        String linhaTratada = tratarLinhaParaProcessamento(linha);

        Cnab150RegistroTipo registro = getRegistro(linhaTratada, 1, 1);
        switch (registro) {
        case CABECALHO:
            CnabInterface secaoProcessada = processarCabecalhoArquivo(linhaTratada);
            secaoProcessada.criarLote();
            return secaoProcessada;
        case DETALHAMENTO:
            return processarDetalhamentoArquivo(linhaTratada);
        case TRAILER:
            return processarTrailerArquivo(linhaTratada);
        default:
            return null;
        }
    }

    protected Cnab150RegistroTipo getRegistro(String str, int posicao, int quantidade) {
        return getRegistroTipo(getString(str, posicao, quantidade));
    }

}
