package br.org.acropole.cnab.domain.cnab400.convenio;

import br.org.acropole.cnab.domain.cnab400.arquivo.Cnab400CabecalhoInterface;

public interface Cnab400CabecalhoConvenioInterface extends Cnab400CabecalhoInterface {

    String getConvenio();

    void setConvenio(String convenio);

    String getSequencialRetorno();

    void setSequencialRetorno(String sequencialRetorno);

}
