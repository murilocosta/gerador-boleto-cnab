package br.org.acropole.cnab.domain.cnab240.arquivo;

import br.org.acropole.cnab.infra.arquivo.CnabTrailerInterface;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public abstract class Cnab240TrailerAbstract extends Cnab240Abstract implements CnabTrailerInterface {

    public <B extends Cnab240TrailerBuilderAbstract<B, E>, E extends Cnab240Abstract> Cnab240TrailerAbstract(B builder) {
        super(builder);
    }

    public abstract static class Cnab240TrailerBuilderAbstract<B extends Cnab240TrailerBuilderAbstract<B, E>, E extends Cnab240Abstract>
            extends Cnab240BuilderAbstract<B, E> {

    }

}
