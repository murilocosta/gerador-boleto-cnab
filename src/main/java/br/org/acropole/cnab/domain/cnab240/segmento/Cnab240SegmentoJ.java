package br.org.acropole.cnab.domain.cnab240.segmento;

import br.org.acropole.cnab.domain.cnab240.arquivo.Cnab240Detalhamento;
import br.org.acropole.cnab.domain.cnab240.arquivo.Cnab240Detalhamento.Cnab240DetalhamentoBuilder;
import br.org.acropole.cnab.domain.modelo.Banco;
import br.org.acropole.cnab.domain.modelo.Cedente;

public class Cnab240SegmentoJ extends Cnab240SegmentoAbstract {

    @Override
    public Cnab240Detalhamento montarDetalhamento(String linha) {
        return new Cnab240DetalhamentoBuilder()
                .cedente(Cedente.builder()
                        .banco(Banco.builder()
                                .codigo(getString(linha, 1, 3))
                                .build())
                        .build())
                .lote(getString(linha, 4, 4))
                .registro(getRegistro(linha, 8, 1))
                .numeroRegistroLote(getString(linha, 9, 5))
                .segmento(getSegmento(linha, 14, 1))
                .tipoMovimentacao(getString(linha, 15, 1))
                .codigoMovimentacao(getString(linha, 16, 2))
                .codigoBarras(getString(linha, 18, 44))
                .dataVencimento(getDate(linha, 92, 8))
                .valorTitulo(getDecimal(linha, 100, 15))
                .valorDesconto(getDecimal(linha, 115, 15))
                .valorAcrescimos(getDecimal(linha, 130, 15))
                .dataPagamento(getDate(linha, 145, 8))
                .valorPagamento(getDecimal(linha, 153, 15))
                .quantidadeMoeda(getDecimal(linha, 168, 15))
                .referenciaSacado(getString(linha, 183, 20))
                .nossoNumero(getString(linha, 203, 20))
                .codigoMoeda(getString(linha, 223, 2))
                .cnab(getString(linha, 225, 6))
                .ocorrencia(getString(linha, 231, 10))
                .build();
    }

}
