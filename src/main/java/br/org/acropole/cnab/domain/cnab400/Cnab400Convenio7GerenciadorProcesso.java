package br.org.acropole.cnab.domain.cnab400;

import br.org.acropole.cnab.domain.GerenciadorProcessoAbstract;
import br.org.acropole.cnab.domain.cnab400.processador.Cnab400Convenio7Processador;
import br.org.acropole.cnab.domain.cnab400.processador.Cnab400ProcessadorRetorno;

public class Cnab400Convenio7GerenciadorProcesso extends GerenciadorProcessoAbstract<Cnab400Convenio7Processador, Cnab400ProcessadorRetorno> {

    public Cnab400Convenio7GerenciadorProcesso(Cnab400Convenio7Processador processadorArquivo, Cnab400ProcessadorRetorno processadorRetorno) {
        super(processadorArquivo, processadorRetorno);
    }

}
