package br.org.acropole.cnab.domain.cnab400.processador;

import br.org.acropole.cnab.domain.cnab400.arquivo.Cnab400CabecalhoInterface;
import br.org.acropole.cnab.domain.cnab400.arquivo.Cnab400DetalhamentoInterface;
import br.org.acropole.cnab.domain.cnab400.arquivo.Cnab400RegistroTipo;
import br.org.acropole.cnab.domain.cnab400.convenio.Cnab400CabecalhoConvenio.Cnab400CabecalhoConvenioBuilder;
import br.org.acropole.cnab.domain.cnab400.convenio.Cnab400DetalhamentoConvenio.Cnab400DetalhamentoConvenioBuilder;

import java.io.File;

/**
 * Classe para leitura de arquivos de retorno de cobranças no padrão CNAB400/CBR643 com convênio de 7 posições.<br/>
 * Layout Padrão CNAB/Febraban 400 posições baseado na documentação para:<br/>
 * "Layout de Arquivo Retorno para convênios na faixa numérica entre 1.000.000 a 9.999.999 (Convênios de 7 posições). Versão Set/09"
 * do Banco do Brasil (arquivo Doc8826BR643Pos6.pdf), disponível em http://www.bb.com.br/docs/pub/emp/empl/dwn/Doc8826BR643Pos6.pdf
 */
public class Cnab400Convenio6Processador extends Cnab400ProcessadorAbstract {

    public Cnab400Convenio6Processador(File arquivo) {
        super(arquivo);
    }

    @Override
    protected Cnab400CabecalhoInterface processarCabecalhoArquivo(String linha) {
        Cnab400CabecalhoConvenioBuilder builder = new Cnab400CabecalhoConvenioBuilder()
                .convenio(getString(linha, 41, 6))
                .sequencialRetorno(getString(linha, 101, 7))
                .complemento(getString(linha, 108, 287));
        return processarCabecalhoArquivo(linha, builder);
    }

    @Override
    protected Cnab400DetalhamentoInterface processadorDetalhamentoArquivo(String linha) {
        return processadorDetalhamentoArquivo(linha, new Cnab400DetalhamentoConvenioBuilder());
    }

    @Override
    protected Cnab400RegistroTipo getDetalhamentoTipo() {
        return Cnab400RegistroTipo.DETALHAMENTO_CONVENIO_6;
    }

}
