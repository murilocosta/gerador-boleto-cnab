package br.org.acropole.cnab.domain.cnab400.arquivo;

import br.org.acropole.cnab.infra.arquivo.CnabRegistroTipo;

public enum Cnab400ColunaTipo implements CnabRegistroTipo {

    ALFANUMERICO("X"), NUMERICO("9"), DECIMAL_COM_VIRGULA("V");

    private String codigo;

    Cnab400ColunaTipo(String codigo) {
        this.codigo = codigo;
    }

    @Override
    public String getCodigo() {
        return codigo;
    }

}
