package br.org.acropole.cnab.domain.cnab240.modelo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;

import java.math.BigDecimal;

@Builder
@ToString
@EqualsAndHashCode
public class Titulo {

    @Getter
    @Setter
    protected BigDecimal acrescimos;

    @Getter
    @Setter
    protected BigDecimal valorDesconto;

    @Getter
    @Setter
    protected BigDecimal valorAbatimento;

    @Getter
    @Setter
    protected BigDecimal valorIof;

    @Getter
    @Setter
    protected BigDecimal valorPago;

    @Getter
    @Setter
    protected BigDecimal valorLiquido;

}
