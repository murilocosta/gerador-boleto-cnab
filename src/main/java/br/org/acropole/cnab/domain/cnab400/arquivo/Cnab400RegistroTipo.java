package br.org.acropole.cnab.domain.cnab400.arquivo;

import br.org.acropole.cnab.infra.arquivo.CnabRegistroTipo;

public enum Cnab400RegistroTipo implements CnabRegistroTipo {

    CABECALHO("0"), DETALHAMENTO_CONVENIO_6("1"), DETALHAMENTO_CONVENIO_7("7"), TRAILER("9");

    private String codigo;

    private Cnab400RegistroTipo(String codigo) {
        this.codigo = codigo;
    }

    @Override
    public String getCodigo() {
        return codigo;
    }

    public static Cnab400RegistroTipo getRegistroTipo(String codigo) {
        for (Cnab400RegistroTipo registroTipo : values()) {
            if (registroTipo.getCodigo().equals(codigo)) {
                return registroTipo;
            }
        }
        return null;
    }
}
