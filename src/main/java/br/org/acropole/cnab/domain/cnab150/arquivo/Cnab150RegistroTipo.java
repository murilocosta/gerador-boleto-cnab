package br.org.acropole.cnab.domain.cnab150.arquivo;

import br.org.acropole.cnab.infra.arquivo.CnabRegistroTipo;

public enum Cnab150RegistroTipo implements CnabRegistroTipo {

    CABECALHO("A"), DETALHAMENTO("G"), TRAILER("Z");

    private String codigo;

    private Cnab150RegistroTipo(String codigo) {
        this.codigo = codigo;
    }

    @Override
    public String getCodigo() {
        return codigo;
    }

    public static Cnab150RegistroTipo getRegistroTipo(String tipo) {
        for (Cnab150RegistroTipo registroTipo : values()) {
            if (registroTipo.getCodigo().equals(tipo)) {
                return registroTipo;
            }
        }
        return null;
    }

}
