package br.org.acropole.cnab.domain.cnab240.segmento;

import br.org.acropole.cnab.domain.cnab240.segmento.tipo.Cnab240SegmentoTipo;

public class Cnab240SegmentoFactory {

    public Cnab240SegmentoInterface getSegmento(Cnab240SegmentoTipo segmentoTipo) {
        Cnab240SegmentoInterface segmento = null;
        switch (segmentoTipo) {
        case SEGMENTO_J:
            segmento = new Cnab240SegmentoJ();
            break;
        case SEGMENTO_T:
            segmento = new Cnab240SegmentoT();
            break;
        case SEGMENTO_U:
            segmento = new Cnab240SegmentoU();
            break;
        }
        return segmento;
    }

}
