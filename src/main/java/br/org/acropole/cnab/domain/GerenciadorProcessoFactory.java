package br.org.acropole.cnab.domain;

import br.org.acropole.cnab.domain.cnab150.Cnab150GerenciadorProcesso;
import br.org.acropole.cnab.domain.cnab150.processador.Cnab150Processador;
import br.org.acropole.cnab.domain.cnab150.processador.Cnab150ProcessadorRetorno;
import br.org.acropole.cnab.domain.cnab240.Cnab240GerenciadorProcesso;
import br.org.acropole.cnab.domain.cnab240.processador.Cnab240Processador;
import br.org.acropole.cnab.domain.cnab240.processador.Cnab240ProcessadorRetorno;
import br.org.acropole.cnab.domain.cnab400.Cnab400Convenio6GerenciadorProcesso;
import br.org.acropole.cnab.domain.cnab400.Cnab400Convenio7GerenciadorProcesso;
import br.org.acropole.cnab.domain.cnab400.arquivo.Cnab400RegistroTipo;
import br.org.acropole.cnab.domain.cnab400.processador.Cnab400Convenio6Processador;
import br.org.acropole.cnab.domain.cnab400.processador.Cnab400Convenio7Processador;
import br.org.acropole.cnab.domain.cnab400.processador.Cnab400ProcessadorRetorno;
import br.org.acropole.cnab.infra.excecao.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

import static br.org.acropole.cnab.domain.cnab400.arquivo.Cnab400RegistroTipo.getRegistroTipo;
import static java.lang.String.valueOf;
import static org.apache.commons.io.IOUtils.readLines;
import static org.apache.commons.lang3.StringUtils.contains;
import static org.apache.commons.lang3.StringUtils.isBlank;

public class GerenciadorProcessoFactory {

    private static final String BANCO_BRADESCO_NAO_SUPORTADO = "BRADESCO";

    public static GerenciadorProcessoAbstract getProcessadorArquivo(File arquivo) throws RetornoArquivoException {
        String cabecalho;
        String detalhamento;
        try {
            Iterator<String> iterador = readLines(new FileInputStream(arquivo)).iterator();
            cabecalho = iterador.next();
            detalhamento = iterador.next();
        } catch (IOException e) {
            throw new RuntimeException("Não foi possível fazer a leitura do arquivo.");
        }
        if (isBlank(cabecalho)) {
            throw new SecaoCabecalhoNaoEncontradaException("Tipo de arquivo de retorno não identificado. Não foi possível ler o cabeçalho do arquivo");
        }
        return getInstancia(arquivo, cabecalho, detalhamento);
    }

    private static GerenciadorProcessoAbstract getInstancia(File arquivo, String cabecalho, String detalhamento) throws RetornoArquivoException {
        if (cabecalho.length() >= 150 && cabecalho.length() <= 152) {
            return new Cnab150GerenciadorProcesso(new Cnab150Processador(arquivo), new Cnab150ProcessadorRetorno());
        } else if (cabecalho.length() >= 240 && cabecalho.length() <= 242) {
            return new Cnab240GerenciadorProcesso(new Cnab240Processador(arquivo), new Cnab240ProcessadorRetorno());
        } else if (cabecalho.length() >= 400 && cabecalho.length() <= 402) {
            if (contains(cabecalho, BANCO_BRADESCO_NAO_SUPORTADO)) {
                throw new ArquivoRetornoNaoSuportadoException("Arquivo de retorno do Bradesco não é suportado");
            }
            if (isBlank(detalhamento)) {
                throw new SecaoDetalhamentoNaoEncontradaException("Tipo de arquivo de retorno não identificado. Não foi possível ler um registro de detalhamento");
            }
            Cnab400RegistroTipo registro = getRegistroTipo(valueOf(detalhamento.charAt(0)));
            switch (registro) {
                case DETALHAMENTO_CONVENIO_6:
                    return new Cnab400Convenio6GerenciadorProcesso(new Cnab400Convenio6Processador(arquivo), new Cnab400ProcessadorRetorno());
                case DETALHAMENTO_CONVENIO_7:
                    return new Cnab400Convenio7GerenciadorProcesso(new Cnab400Convenio7Processador(arquivo), new Cnab400ProcessadorRetorno());
                default:
                    throw new RuntimeException("Tipo de registro de detalhamento desconhecido: " + registro);
            }
        } else {
            throw new CabecalhoInvalidoException("Tipo de arquivo de retorno não identificado. Total de colunas do cabeçalho: " + cabecalho.length());
        }
    }

}
