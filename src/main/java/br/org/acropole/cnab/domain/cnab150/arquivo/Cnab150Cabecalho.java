package br.org.acropole.cnab.domain.cnab150.arquivo;

import br.org.acropole.cnab.infra.arquivo.CnabCabecalhoInterface;
import lombok.*;

import java.util.Date;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Cnab150Cabecalho extends Cnab150Abstract implements CnabCabecalhoInterface {

    @Getter
    @Setter
    protected String remessa;

    @Getter
    @Setter
    protected String convenio;

    @Getter
    @Setter
    protected Date dataGeracao;

    @Getter
    @Setter
    protected String sequencialRetorno;

    @Getter
    @Setter
    protected String versaoLayout;

    public Cnab150Cabecalho(Cnab150CabecalhoBuilder builder) {
        super(builder);
        this.remessa = builder.remessa;
        this.convenio = builder.convenio;
        this.dataGeracao = builder.dataGeracao;
        this.sequencialRetorno = builder.sequencialRetorno;
        this.versaoLayout = builder.versaoLayout;
    }

    public static class Cnab150CabecalhoBuilder extends Cnab150BuilderAbstract<Cnab150CabecalhoBuilder, Cnab150Cabecalho> {

        protected String remessa;
        protected String convenio;
        protected Date dataGeracao;
        protected String sequencialRetorno;
        protected String versaoLayout;

        public Cnab150CabecalhoBuilder remessa(String remessa) {
            this.remessa = remessa;
            return getThis();
        }

        public Cnab150CabecalhoBuilder convenio(String convenio) {
            this.convenio = convenio;
            return getThis();
        }

        public Cnab150CabecalhoBuilder dataGeracao(Date dataGeracao) {
            this.dataGeracao = dataGeracao;
            return getThis();
        }

        public Cnab150CabecalhoBuilder sequencialRetorno(String sequencialRetorno) {
            this.sequencialRetorno = sequencialRetorno;
            return getThis();
        }

        public Cnab150CabecalhoBuilder versaoLayout(String versaoLayout) {
            this.versaoLayout = versaoLayout;
            return getThis();
        }

        @Override
        public Cnab150CabecalhoBuilder getThis() {
            return this;
        }

        @Override
        public Cnab150Cabecalho build() {
            return new Cnab150Cabecalho(this);
        }

    }

}
