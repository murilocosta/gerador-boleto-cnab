package br.org.acropole.cnab.domain.modelo;

import lombok.*;

@ToString
@EqualsAndHashCode
@AllArgsConstructor
public abstract class Pessoa {

    @Getter
    @Setter
    protected String nome;

    @Getter
    @Setter
    protected Banco banco;

}
