package br.org.acropole.cnab.domain.cnab240.segmento;

import br.org.acropole.cnab.domain.cnab240.arquivo.Cnab240Detalhamento;
import br.org.acropole.cnab.domain.cnab240.arquivo.Cnab240Detalhamento.Cnab240DetalhamentoBuilder;
import br.org.acropole.cnab.domain.cnab240.modelo.Titulo;
import br.org.acropole.cnab.domain.modelo.Banco;
import br.org.acropole.cnab.domain.modelo.Cedente;
import br.org.acropole.cnab.domain.modelo.Ocorrencia;
import br.org.acropole.cnab.domain.modelo.Sacado;

public class Cnab240SegmentoU extends Cnab240SegmentoAbstract {

    @Override
    public Cnab240Detalhamento montarDetalhamento(String linha) {
        return new Cnab240DetalhamentoBuilder()
                .sacado(Sacado.builder()
                        .banco(Banco.builder()
                                .codigo(getString(linha, 1, 3))
                                .build())
                        .build())
                .lote(getString(linha, 4, 4))
                .registro(getRegistro(linha, 8, 1))
                .numeroRegistroLote(getString(linha, 9, 5))
                .segmento(getSegmento(linha, 14, 1))
                .cnab(getString(linha, 15, 1))
                .codigoMovimentacao(getString(linha, 16, 2))
                .titulo(Titulo.builder().acrescimos(getDecimal(linha, 18, 15)).valorDesconto(getDecimal(linha, 33, 15))
                        .valorAbatimento(getDecimal(linha, 48, 15)).valorIof(getDecimal(linha, 63, 15)).valorPago(getDecimal(linha, 78, 15))
                        .valorLiquido(getDecimal(linha, 93, 15)).build())
                .outrasDespesas(getDecimal(linha, 108, 15))
                .outrosCreditos(getDecimal(linha, 123, 15))
                .dataOcorrencia(getDate(linha, 138, 8))
                .dataCredito(getDate(linha, 146, 8))
                .ocorrencia(Ocorrencia.builder()
                        .codigo(getInteger(linha, 154, 4))
                        .data(getDate(linha, 158, 8))
                        .valor(getDecimal(linha, 166, 15))
                        .complemento(getString(linha, 181, 30))
                        .build())
                .cedente(Cedente.builder()
                        .banco(Banco.builder()
                                .codigo(getString(linha, 211, 3))
                                .build())
                        .build())
                .nossoNumero(getString(linha, 214, 20))
                .cnab(getString(linha, 234, 7))
                .build();
    }

}
