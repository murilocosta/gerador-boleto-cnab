package br.org.acropole.cnab.domain.cnab240.arquivo.lote;

import br.org.acropole.cnab.domain.cnab240.arquivo.Cnab240TrailerAbstract;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Cnab240TrailerLote extends Cnab240TrailerAbstract {

    @Getter
    @Setter
    protected Integer quantidadeRegistros;

    @Getter
    @Setter
    protected BigDecimal valor;

    @Getter
    @Setter
    protected BigDecimal quantidadeMoedas;

    @Getter
    @Setter
    protected String numeroAvisoDebito;

    public Cnab240TrailerLote(Cnab240TrailerLoteBuilder builder) {
        super(builder);
        this.quantidadeRegistros = builder.quantidadeRegistros;
        this.valor = builder.valor;
        this.quantidadeMoedas = builder.quantidadeMoedas;
        this.numeroAvisoDebito = builder.numeroAvisoDebito;
    }

    public static class Cnab240TrailerLoteBuilder extends Cnab240TrailerBuilderAbstract<Cnab240TrailerLoteBuilder, Cnab240TrailerLote> {

        protected Integer quantidadeRegistros;
        protected BigDecimal valor;
        protected BigDecimal quantidadeMoedas;
        protected String numeroAvisoDebito;

        public Cnab240TrailerLoteBuilder quantidadeRegistros(Integer quantidadeRegistros) {
            this.quantidadeRegistros = quantidadeRegistros;
            return getThis();
        }

        public Cnab240TrailerLoteBuilder valor(BigDecimal valor) {
            this.valor = valor;
            return getThis();
        }

        public Cnab240TrailerLoteBuilder quantidadeMoedas(BigDecimal quantidadeMoedas) {
            this.quantidadeMoedas = quantidadeMoedas;
            return getThis();
        }

        public Cnab240TrailerLoteBuilder numeroAvisoDebito(String numeroAvisoDebito) {
            this.numeroAvisoDebito = numeroAvisoDebito;
            return getThis();
        }

        @Override
        public Cnab240TrailerLoteBuilder getThis() {
            return this;
        }

        @Override
        public Cnab240TrailerLote build() {
            return new Cnab240TrailerLote(this);
        }

    }

}
