package br.org.acropole.cnab.domain.cnab240.processador;

import br.org.acropole.cnab.domain.cnab240.arquivo.Cnab240RegistroTipo;
import br.org.acropole.cnab.infra.ProcessadorRetornoAbstract;
import br.org.acropole.cnab.infra.arquivo.CnabCabecalhoInterface;
import br.org.acropole.cnab.infra.arquivo.CnabDetalhamentoInterface;
import br.org.acropole.cnab.infra.arquivo.CnabInterface;
import br.org.acropole.cnab.infra.arquivo.CnabTrailerInterface;
import br.org.acropole.cnab.infra.lote.LoteInterface;
import br.org.acropole.cnab.infra.retorno.RetornoInterface;

import static br.org.acropole.cnab.domain.cnab240.arquivo.Cnab240RegistroTipo.getColunaTipo;

public class Cnab240ProcessadorRetorno extends ProcessadorRetornoAbstract {

    @Override
    protected boolean processarRetorno(RetornoInterface retorno, CnabInterface composicao) {
        Cnab240RegistroTipo colunaTipo = getColunaTipo(composicao.getRegistro().getCodigo());
        switch (colunaTipo) {
            case CABECALHO:
                retorno.setCabecalho(CnabCabecalhoInterface.class.cast(composicao));
                return true;

            case TRAILER:
                retorno.setTrailer(CnabTrailerInterface.class.cast(composicao));
                return true;

            default:
                return false;
        }
    }

    @Override
    protected void processarLote(LoteInterface lote, CnabInterface composicao) {
        Cnab240RegistroTipo colunaTipo = getColunaTipo(composicao.getRegistro().getCodigo());
        switch (colunaTipo) {
            case CABECALHO_LOTE:
                if (CnabCabecalhoInterface.class.isInstance(composicao)) {
                    lote.setCabecalho(CnabCabecalhoInterface.class.cast(composicao));
                } else {
                    lote.adicionarDetalhamento(CnabDetalhamentoInterface.class.cast(composicao));
                }
                break;

            case TRAILER_LOTE:
                lote.setTrailer(CnabTrailerInterface.class.cast(composicao));
                break;

            default:
                lote.adicionarDetalhamento(CnabDetalhamentoInterface.class.cast(composicao));
                break;
        }
    }

}
