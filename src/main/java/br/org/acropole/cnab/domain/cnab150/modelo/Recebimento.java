package br.org.acropole.cnab.domain.cnab150.modelo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Builder;

import java.math.BigDecimal;
import java.util.Date;

@Builder
@ToString
@EqualsAndHashCode
public class Recebimento {

    @Getter
    @Setter
    protected Date dataCredito;

    @Getter
    @Setter
    protected BigDecimal valorRecebido;

    @Getter
    @Setter
    protected BigDecimal valorTarifa;

    @Getter
    @Setter
    protected String numeroSequencial;

    @Getter
    @Setter
    protected String codigoAgenciaArrecadadora;

    @Getter
    @Setter
    protected String formaArrecadacao;

    @Getter
    @Setter
    protected String numeroAutenticacao;

}
