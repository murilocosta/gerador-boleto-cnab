package br.org.acropole.cnab.domain.cnab400.arquivo;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Cnab400Cabecalho extends Cnab400CabecalhoAbstract {

    public Cnab400Cabecalho(Cnab400CabecalhoBuilder builder) {
        super(builder);
    }

    public static class Cnab400CabecalhoBuilder extends Cnab400CabecalhoBuilderAbstract<Cnab400CabecalhoBuilder, Cnab400Cabecalho> {

        @Override
        public Cnab400CabecalhoBuilder getThis() {
            return this;
        }

        @Override
        public Cnab400Cabecalho build() {
            return new Cnab400Cabecalho(this);
        }

    }

}
