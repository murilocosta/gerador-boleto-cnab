package br.org.acropole.cnab.domain;

import br.org.acropole.cnab.infra.ProcessadorArquivoAbstract;
import br.org.acropole.cnab.infra.ProcessadorRetornoInterface;
import br.org.acropole.cnab.infra.arquivo.CnabInterface;
import br.org.acropole.cnab.infra.retorno.RetornoInterface;
import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.util.Collection;
import java.util.HashSet;

public abstract class GerenciadorProcessoAbstract<A extends ProcessadorArquivoAbstract, R extends ProcessadorRetornoInterface> {

    protected A processadorArquivo;

    protected R processadorRetorno;

    public GerenciadorProcessoAbstract(A processadorArquivo, R processadorRetorno) {
        this.processadorArquivo = processadorArquivo;
        this.processadorRetorno = processadorRetorno;
    }

    public RetornoInterface processar() throws Exception {
        Collection<CnabInterface> componentes = new HashSet<CnabInterface>();
        for (String linha : IOUtils.readLines(new FileInputStream(getProcessadorArquivo().getArquivo()))) {
            componentes.add(getProcessadorArquivo().processarLinha(linha));
        }
        return getProcessadorRetorno().executar(componentes);
    }

    public A getProcessadorArquivo() {
        return processadorArquivo;
    }

    public R getProcessadorRetorno() {
        return processadorRetorno;
    }
}
