package br.org.acropole.cnab;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"br.org.acropole.cnab.domain", "br.org.acropole.cnab.infra"})
public class RetornoBoletoContext {

}
