package br.org.acropole.cnab;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public final class CentralRetornoBoleto {

    private static ApplicationContext applicationContext;

    private CentralRetornoBoleto() {

    }

    private static ApplicationContext getApplicationContext() {
        if (applicationContext == null) {
            applicationContext = new AnnotationConfigApplicationContext(RetornoBoletoContext.class);
        }
        return applicationContext;
    }

}
