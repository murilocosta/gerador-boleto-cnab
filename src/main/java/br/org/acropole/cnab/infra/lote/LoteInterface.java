package br.org.acropole.cnab.infra.lote;

import br.org.acropole.cnab.infra.arquivo.CnabCabecalhoInterface;
import br.org.acropole.cnab.infra.arquivo.CnabDetalhamentoInterface;
import br.org.acropole.cnab.infra.arquivo.CnabTrailerInterface;

import java.util.Collection;

public interface LoteInterface {

    CnabCabecalhoInterface getCabecalho();

    void setCabecalho(CnabCabecalhoInterface cabecalho);

    Collection<CnabDetalhamentoInterface> getDetalhamentos();

    void setDetalhamentos(Collection<CnabDetalhamentoInterface> detalhamentos);

    LoteInterface adicionarDetalhamento(CnabDetalhamentoInterface detalhamento);

    LoteInterface excluirDetalhamento(CnabDetalhamentoInterface detalhamento);

    CnabTrailerInterface getTrailer();

    void setTrailer(CnabTrailerInterface trailer);

}
