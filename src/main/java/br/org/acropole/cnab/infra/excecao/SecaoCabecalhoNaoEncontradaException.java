package br.org.acropole.cnab.infra.excecao;

public class SecaoCabecalhoNaoEncontradaException extends RetornoArquivoException {

    public SecaoCabecalhoNaoEncontradaException(String message) {
        super(message);
    }

}
