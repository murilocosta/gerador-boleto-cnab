package br.org.acropole.cnab.infra.retorno;

import br.org.acropole.cnab.infra.arquivo.CnabCabecalhoInterface;
import br.org.acropole.cnab.infra.arquivo.CnabTrailerInterface;
import br.org.acropole.cnab.infra.lote.LoteInterface;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Collection;
import java.util.HashSet;

@ToString
@EqualsAndHashCode
public class Retorno implements RetornoInterface {

    private CnabCabecalhoInterface cabecalho;
    private Collection<LoteInterface> lotes;
    private CnabTrailerInterface trailer;

    public Retorno() {
        this.lotes = new HashSet<>();
    }

    @Override
    public CnabCabecalhoInterface getCabecalho() {
        return cabecalho;
    }

    @Override
    public void setCabecalho(CnabCabecalhoInterface cabecalho) {
        this.cabecalho = cabecalho;
    }

    @Override
    public Collection<LoteInterface> getLotes() {
        return lotes;
    }

    @Override
    public void setLotes(Collection<LoteInterface> lotes) {
        this.lotes = lotes;
    }

    @Override
    public RetornoInterface adicionarLote(LoteInterface lote) {
        lotes.add(lote);
        return this;
    }

    @Override
    public RetornoInterface excluirLote(LoteInterface lote) {
        if (lotes.contains(lote)) {
            lotes.remove(lote);
        }
        return this;
    }

    @Override
    public CnabTrailerInterface getTrailer() {
        return trailer;
    }

    @Override
    public void setTrailer(CnabTrailerInterface trailer) {
        this.trailer = trailer;
    }

}
