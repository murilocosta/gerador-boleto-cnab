package br.org.acropole.cnab.infra;

import lombok.Getter;

import java.io.File;

public abstract class ProcessadorArquivoAbstract extends DecodificadorLinhaAbstract implements ProcessadorArquivoInterface {

    @Getter
    protected File arquivo;

    public ProcessadorArquivoAbstract(File arquivo) {
        this.arquivo = arquivo;
    }

    protected String tratarLinhaParaProcessamento(String linha) {
        return new StringBuilder(linha).insert(0, " ").toString().replace("\n\r", "");
    }

}
