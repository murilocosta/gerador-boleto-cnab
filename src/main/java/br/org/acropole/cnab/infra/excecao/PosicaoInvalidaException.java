package br.org.acropole.cnab.infra.excecao;

public class PosicaoInvalidaException extends RetornoArquivoException {

    public PosicaoInvalidaException(String message) {
        super(message);
    }

}
