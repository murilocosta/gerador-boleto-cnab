package br.org.acropole.cnab.infra.arquivo;

import br.org.acropole.cnab.domain.modelo.Cedente;
import br.org.acropole.cnab.domain.modelo.Empresa;
import br.org.acropole.cnab.domain.modelo.Sacado;
import br.org.acropole.cnab.infra.builder.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Collection;
import java.util.HashSet;

@ToString
@EqualsAndHashCode
public abstract class CnabAbstract implements CnabInterface {

    @Getter
    @Setter
    protected CnabRegistroTipo registro;

    @Getter
    @Setter
    protected String lote;

    @Getter
    @Setter
    protected Empresa empresa;

    @Getter
    @Setter
    protected Cedente cedente;

    @Getter
    @Setter
    protected Sacado sacado;

    @Getter
    @Setter
    protected Collection<String> cnabs = new HashSet<String>();

    @Getter
    @Setter
    protected Collection<String> ocorrencias = new HashSet<String>();

    private boolean criarLote = false;

    @Override
    public void criarLote() {
        criarLote = true;
    }

    @Override
    public boolean isCriarLote() {
        return criarLote;
    }

    public <B extends CnabBuilderAbstract<B, E>, E extends CnabAbstract> CnabAbstract(B builder) {
        this.registro = builder.registro;
        this.lote = builder.lote;
        this.empresa = builder.empresa;
        this.cedente = builder.cedente;
        this.sacado = builder.sacado;
        this.cnabs = builder.cnabs;
        this.ocorrencias = builder.ocorrencias;
    }

    @Override
    public void adicionarCnab(String cnab) {
        cnabs.add(cnab);
    }

    @Override
    public void excluirCnab(String cnab) {
        if (cnabs.contains(cnab)) {
            cnabs.add(cnab);
        }
    }

    @Override
    public void adicionarOcorrencia(String ocorrencia) {
        ocorrencias.add(ocorrencia);
    }

    @Override
    public void excluirOcorrencia(String ocorrencia) {
        if (ocorrencias.contains(ocorrencia)) {
            ocorrencias.remove(ocorrencia);
        }
    }

    public abstract static class CnabBuilderAbstract<B extends CnabBuilderAbstract<B, T>, T extends CnabAbstract> implements Builder<B, T> {

        protected CnabRegistroTipo registro;
        protected String lote;
        protected Empresa empresa;
        protected Cedente cedente;
        protected Sacado sacado;
        protected Collection<String> cnabs;
        protected Collection<String> ocorrencias;


        public B registro(CnabRegistroTipo registro) {
            this.registro = registro;
            return getThis();
        }

        public B lote(String lote) {
            this.lote = lote;
            return getThis();
        }

        public B empresa(Empresa empresa) {
            this.empresa = empresa;
            return getThis();
        }

        public B cedente(Cedente cedente) {
            this.cedente = cedente;
            return getThis();
        }

        public B sacado(Sacado sacado) {
            this.sacado = sacado;
            return getThis();
        }

        public B cnabs(Collection<String> cnabs) {
            this.cnabs = cnabs;
            return getThis();
        }

        public B cnab(String cnab) {
            this.cnabs = (this.cnabs == null) ? new HashSet<String>() : this.cnabs;
            this.cnabs.add(cnab);
            return getThis();
        }

        public B ocorrencias(Collection<String> ocorrencias) {
            this.ocorrencias = ocorrencias;
            return getThis();
        }

        public B ocorrencia(String ocorrencia) {
            this.ocorrencias = (this.ocorrencias == null) ? new HashSet<String>() : this.ocorrencias;
            this.ocorrencias.add(ocorrencia);
            return getThis();
        }

    }

}
