package br.org.acropole.cnab.infra;

import br.org.acropole.cnab.infra.arquivo.CnabInterface;

public interface ProcessadorArquivoInterface {

    CnabInterface processarLinha(String linha);

}
