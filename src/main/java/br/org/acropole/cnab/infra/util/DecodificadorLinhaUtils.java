package br.org.acropole.cnab.infra.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.SimpleTypeConverter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DecodificadorLinhaUtils {

    public static String getString(String str, int inicio, int quantidade) {
        return str.substring(inicio, inicio + quantidade).trim();
    }

    public static Integer getInteger(String str, int inicio, int quantidade) {
        return new SimpleTypeConverter().convertIfNecessary(getString(str, inicio, quantidade), Integer.class);
    }

    public static BigDecimal getDecimal(String str, int inicio, int quantidade, int escala) {
        return getDecimal(getString(str, inicio, quantidade), escala);
    }

    public static BigDecimal getDecimal(String valor, int escala) {
        String valorDecimal = formatarDecimal(valor, escala);
        BigDecimal valorConvertido = new SimpleTypeConverter().convertIfNecessary(valorDecimal, BigDecimal.class);
        return valorConvertido.setScale(escala, RoundingMode.HALF_EVEN);
    }

    private static String formatarDecimal(String valor, int escala) {
        Matcher matcher = Pattern.compile("(\\d*)(\\d{" + escala + "})$").matcher(valor);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Invalid number format!");
        }
        MatchResult resultado = matcher.toMatchResult();
        return resultado.group(1) + "." + resultado.group(2);
    }

    public static Date getDate(String str, int inicio, int quantidade, String formato) {
        return formatarData(getString(str, inicio, quantidade), formato);
    }

    private static Date formatarData(String data, String formato) {
        if (StringUtils.isBlank(data)) {
            return null;
        }
        Date valorConvertido;
        try {
            DateFormat simpleDateFormat = new SimpleDateFormat(formato);
            valorConvertido = simpleDateFormat.parse(data);
        } catch (ParseException e) {
            throw new RuntimeException(e.getMessage());
        }
        return valorConvertido;
    }

}
