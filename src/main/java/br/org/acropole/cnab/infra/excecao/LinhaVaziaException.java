package br.org.acropole.cnab.infra.excecao;

public class LinhaVaziaException extends RetornoArquivoException {

    public LinhaVaziaException(String message) {
        super(message);
    }

}
