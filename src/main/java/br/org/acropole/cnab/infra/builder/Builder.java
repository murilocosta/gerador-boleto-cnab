package br.org.acropole.cnab.infra.builder;

import br.org.acropole.cnab.infra.arquivo.CnabInterface;

public interface Builder<B extends Builder<B, T>, T extends CnabInterface> {

    B getThis();

    T build();

}
