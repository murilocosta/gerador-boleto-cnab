package br.org.acropole.cnab.infra.arquivo;

import br.org.acropole.cnab.domain.modelo.Cedente;

public interface CnabCabecalhoInterface extends CnabInterface {

    Cedente getCedente();

    void setCedente(Cedente cedente);

}
