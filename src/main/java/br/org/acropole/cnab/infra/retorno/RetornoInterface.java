package br.org.acropole.cnab.infra.retorno;

import br.org.acropole.cnab.infra.arquivo.CnabCabecalhoInterface;
import br.org.acropole.cnab.infra.arquivo.CnabTrailerInterface;
import br.org.acropole.cnab.infra.lote.LoteInterface;

import java.util.Collection;

public interface RetornoInterface {

    CnabCabecalhoInterface getCabecalho();

    void setCabecalho(CnabCabecalhoInterface cabecalho);

    Collection<LoteInterface> getLotes();

    void setLotes(Collection<LoteInterface> lotes);

    RetornoInterface adicionarLote(LoteInterface lote);

    RetornoInterface excluirLote(LoteInterface lote);

    CnabTrailerInterface getTrailer();

    void setTrailer(CnabTrailerInterface trailer);

}
