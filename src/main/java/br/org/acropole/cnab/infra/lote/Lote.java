package br.org.acropole.cnab.infra.lote;

import br.org.acropole.cnab.infra.arquivo.CnabCabecalhoInterface;
import br.org.acropole.cnab.infra.arquivo.CnabDetalhamentoInterface;
import br.org.acropole.cnab.infra.arquivo.CnabTrailerInterface;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Collection;
import java.util.HashSet;

@ToString
@EqualsAndHashCode
public class Lote implements LoteInterface {

    private CnabCabecalhoInterface cabecalho;
    private Collection<CnabDetalhamentoInterface> detalhamentos;
    private CnabTrailerInterface trailer;

    public Lote() {
        this.detalhamentos = new HashSet<>();
    }

    @Override
    public CnabCabecalhoInterface getCabecalho() {
        return cabecalho;
    }

    @Override
    public void setCabecalho(CnabCabecalhoInterface cabecalho) {
        this.cabecalho = cabecalho;
    }

    public Collection<CnabDetalhamentoInterface> getDetalhamentos() {
        return detalhamentos;
    }

    @Override
    public void setDetalhamentos(Collection<CnabDetalhamentoInterface> detalhamentos) {
        this.detalhamentos = detalhamentos;
    }

    @Override
    public LoteInterface adicionarDetalhamento(CnabDetalhamentoInterface detalhamento) {
        detalhamentos.add(detalhamento);
        return this;
    }

    @Override
    public LoteInterface excluirDetalhamento(CnabDetalhamentoInterface detalhamento) {
        if (!detalhamentos.contains(detalhamento)) {
            detalhamentos.remove(detalhamento);
        }
        return this;
    }

    @Override
    public CnabTrailerInterface getTrailer() {
        return trailer;
    }

    @Override
    public void setTrailer(CnabTrailerInterface trailer) {
        this.trailer = trailer;
    }

}
