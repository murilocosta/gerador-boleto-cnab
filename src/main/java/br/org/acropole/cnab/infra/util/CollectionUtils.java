package br.org.acropole.cnab.infra.util;

import java.util.Collection;
import java.util.HashSet;

public class CollectionUtils {

    public static Collection<String> criarColecaoComElemento(String elemento) {
        Collection<String> colecao = new HashSet<String>();
        colecao.add(elemento);
        return colecao;
    }

}
