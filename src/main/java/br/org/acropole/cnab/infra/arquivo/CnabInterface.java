package br.org.acropole.cnab.infra.arquivo;

import br.org.acropole.cnab.domain.modelo.Cedente;
import br.org.acropole.cnab.domain.modelo.Empresa;

import java.util.Collection;

public interface CnabInterface {

    Collection<String> getCnabs();

    void setCnabs(Collection<String> cnabs);

    void adicionarCnab(String cnab);

    void excluirCnab(String cnab);

    CnabRegistroTipo getRegistro();

    void setRegistro(CnabRegistroTipo registro);

    String getLote();

    void setLote(String lote);

    Cedente getCedente();

    void setCedente(Cedente cedente);

    Empresa getEmpresa();

    void setEmpresa(Empresa empresa);

    Collection<String> getOcorrencias();

    void setOcorrencias(Collection<String> ocorrencias);

    void adicionarOcorrencia(String ocorrencia);

    void excluirOcorrencia(String ocorrencia);

    void criarLote();

    boolean isCriarLote();

}
