package br.org.acropole.cnab.infra.excecao;

public class SecaoDetalhamentoNaoEncontradaException extends RetornoArquivoException {

    public SecaoDetalhamentoNaoEncontradaException(String message) {
        super(message);
    }

}
