package br.org.acropole.cnab.infra;

import br.org.acropole.cnab.infra.arquivo.CnabInterface;
import br.org.acropole.cnab.infra.retorno.RetornoInterface;

import java.util.Collection;

public interface ProcessadorRetornoInterface {

    RetornoInterface executar(Collection<CnabInterface> composicoes);

}
