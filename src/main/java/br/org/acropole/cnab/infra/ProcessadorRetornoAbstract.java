package br.org.acropole.cnab.infra;

import br.org.acropole.cnab.infra.arquivo.CnabInterface;
import br.org.acropole.cnab.infra.lote.Lote;
import br.org.acropole.cnab.infra.lote.LoteInterface;
import br.org.acropole.cnab.infra.retorno.Retorno;
import br.org.acropole.cnab.infra.retorno.RetornoInterface;

import java.util.Collection;

public abstract class ProcessadorRetornoAbstract implements ProcessadorRetornoInterface {

    @Override
    public RetornoInterface executar(Collection<CnabInterface> composicoes) {
        RetornoInterface retorno = new Retorno();
        LoteInterface lote = null;
        for (CnabInterface composicao : composicoes) {
            if (composicao.isCriarLote()) {
                lote = new Lote();
                retorno.adicionarLote(lote);
            }
            if (!processarRetorno(retorno, composicao)) {
                if (lote != null) {
                    processarLote(lote, composicao);
                }
            }
        }
        return retorno;
    }

    protected abstract boolean processarRetorno(RetornoInterface retorno, CnabInterface composicao);

    protected abstract void processarLote(LoteInterface lote, CnabInterface composicao);

}
