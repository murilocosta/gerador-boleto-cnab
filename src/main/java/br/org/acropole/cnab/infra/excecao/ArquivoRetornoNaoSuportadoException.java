package br.org.acropole.cnab.infra.excecao;

public class ArquivoRetornoNaoSuportadoException extends RetornoArquivoException {

    public ArquivoRetornoNaoSuportadoException(String message) {
        super(message);
    }

}
