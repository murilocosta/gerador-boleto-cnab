package br.org.acropole.cnab.infra.excecao;

public class CabecalhoInvalidoException extends RetornoArquivoException {

    public CabecalhoInvalidoException(String message) {
        super(message);
    }

}
