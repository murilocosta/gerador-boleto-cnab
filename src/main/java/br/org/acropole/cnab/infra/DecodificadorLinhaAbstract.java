package br.org.acropole.cnab.infra;

import br.org.acropole.cnab.infra.util.DecodificadorLinhaUtils;

import java.math.BigDecimal;
import java.util.Date;

public abstract class DecodificadorLinhaAbstract {

    protected static final String FORMATO_DATE_PADRAO = "yyyyMMdd";
    protected static final String FORMATO_DATE_TIME_PADRAO = "ddMMyyyyHHmmss";
    protected static final Integer ESCALA_DECIMAL_PADRAO = 2;

    protected String getString(String str, int inicio, int quantidade) {
        return DecodificadorLinhaUtils.getString(str, inicio, quantidade);
    }

    protected Integer getInteger(String str, int inicio, int quantidade) {
        return DecodificadorLinhaUtils.getInteger(str, inicio, quantidade);
    }

    protected Date getDate(String str, int inicio, int quantidade) {
        return getDate(str, inicio, quantidade, FORMATO_DATE_PADRAO);
    }

    protected Date getDate(String str, int inicio, int quantidade, String formato) {
        return DecodificadorLinhaUtils.getDate(str, inicio, quantidade, formato);
    }

    protected Date getDateTime(String str, int inicio, int quantidade) {
        return DecodificadorLinhaUtils.getDate(str, inicio, quantidade, FORMATO_DATE_TIME_PADRAO);
    }

    protected BigDecimal getDecimal(String str, int inicio, int quantidade) {
        return DecodificadorLinhaUtils.getDecimal(str, inicio, quantidade, ESCALA_DECIMAL_PADRAO);
    }

    protected BigDecimal getDecimal(String str, int inicio, int quantidade, Integer escala) {
        return DecodificadorLinhaUtils.getDecimal(str, inicio, quantidade, escala);
    }

}
