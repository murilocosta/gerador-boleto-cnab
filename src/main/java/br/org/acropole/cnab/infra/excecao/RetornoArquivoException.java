package br.org.acropole.cnab.infra.excecao;

public abstract class RetornoArquivoException extends Exception {

    public RetornoArquivoException(String message) {
        super(message);
    }

}
