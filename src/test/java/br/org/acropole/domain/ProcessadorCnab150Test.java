package br.org.acropole.domain;

import br.org.acropole.cnab.domain.GerenciadorProcessoAbstract;
import br.org.acropole.cnab.domain.GerenciadorProcessoFactory;
import br.org.acropole.cnab.domain.cnab150.Cnab150GerenciadorProcesso;
import br.org.acropole.cnab.domain.cnab150.processador.Cnab150Processador;
import br.org.acropole.cnab.domain.cnab150.processador.Cnab150ProcessadorRetorno;
import br.org.acropole.cnab.infra.retorno.RetornoInterface;
import br.org.acropole.infra.RetornoBoletoTestAbstract;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class ProcessadorCnab150Test extends RetornoBoletoTestAbstract {

    @Test
    public void deveProcessarUmArquivoCnab150() throws Exception{
        File arquivoRetorno = getApplicationContext().getResource("classpath:ret/150/CNAB150-TEST-001.ret").getFile();
        Cnab150Processador processador = new Cnab150Processador(arquivoRetorno);
        GerenciadorProcessoAbstract gerenciadorProcesso = new Cnab150GerenciadorProcesso(processador, new Cnab150ProcessadorRetorno());
        RetornoInterface retorno = gerenciadorProcesso.processar();
        Assert.assertNotNull(retorno);
    }

    @Test
    public void deveProcessarUmArquivoCnab150UsandoFactory() throws Exception {
        File arquivoRetorno = getApplicationContext().getResource("classpath:ret/150/CNAB150-TEST-002.ret").getFile();
        GerenciadorProcessoAbstract gerenciadorProcesso = GerenciadorProcessoFactory.getProcessadorArquivo(arquivoRetorno);
        Assert.assertThat(gerenciadorProcesso, CoreMatchers.instanceOf(Cnab150GerenciadorProcesso.class));
        RetornoInterface retorno = gerenciadorProcesso.processar();
        Assert.assertNotNull(retorno);
    }

}
