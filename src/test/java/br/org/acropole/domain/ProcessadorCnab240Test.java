package br.org.acropole.domain;

import br.org.acropole.cnab.domain.GerenciadorProcessoAbstract;
import br.org.acropole.cnab.domain.GerenciadorProcessoFactory;
import br.org.acropole.cnab.domain.cnab240.Cnab240GerenciadorProcesso;
import br.org.acropole.cnab.domain.cnab240.processador.Cnab240Processador;
import br.org.acropole.cnab.domain.cnab240.processador.Cnab240ProcessadorRetorno;
import br.org.acropole.cnab.infra.retorno.RetornoInterface;
import br.org.acropole.infra.RetornoBoletoTestAbstract;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class ProcessadorCnab240Test extends RetornoBoletoTestAbstract {

    @Test
    public void deveProcessarUmArquivoCnab240() throws Exception {
        File arquivoRetorno = getApplicationContext().getResource("classpath:ret/240/CNAB240-TEST-006.ret").getFile();
        Cnab240Processador processador = new Cnab240Processador(arquivoRetorno);
        GerenciadorProcessoAbstract gerenciadorProcesso = new Cnab240GerenciadorProcesso(processador, new Cnab240ProcessadorRetorno());
        RetornoInterface retorno = gerenciadorProcesso.processar();
        Assert.assertNotNull(retorno);
    }

    @Test
    public void deveProcessarUmArquivoCnab240UsandoFactory() throws Exception {
        File arquivoRetorno = getApplicationContext().getResource("classpath:ret/240/CNAB240-TEST-002.ret").getFile();
        GerenciadorProcessoAbstract gerenciadorProcesso = GerenciadorProcessoFactory.getProcessadorArquivo(arquivoRetorno);
        Assert.assertThat(gerenciadorProcesso, CoreMatchers.instanceOf(Cnab240GerenciadorProcesso.class));
        RetornoInterface retorno = gerenciadorProcesso.processar();
        Assert.assertNotNull(retorno);
    }

}
