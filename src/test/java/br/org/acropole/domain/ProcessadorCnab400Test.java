package br.org.acropole.domain;

import br.org.acropole.cnab.domain.GerenciadorProcessoAbstract;
import br.org.acropole.cnab.domain.GerenciadorProcessoFactory;
import br.org.acropole.cnab.domain.cnab400.Cnab400Convenio6GerenciadorProcesso;
import br.org.acropole.cnab.domain.cnab400.Cnab400Convenio7GerenciadorProcesso;
import br.org.acropole.cnab.domain.cnab400.processador.Cnab400Convenio6Processador;
import br.org.acropole.cnab.domain.cnab400.processador.Cnab400Convenio7Processador;
import br.org.acropole.cnab.domain.cnab400.processador.Cnab400ProcessadorRetorno;
import br.org.acropole.cnab.infra.retorno.RetornoInterface;
import br.org.acropole.infra.RetornoBoletoTestAbstract;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class ProcessadorCnab400Test extends RetornoBoletoTestAbstract {

    @Test
    public void deveProcessarUmArquivoCnab400Conv6() throws Exception {
        File arquivoRetorno = getApplicationContext().getResource("classpath:ret/400/CNAB400-CONV6-TEST-001.ret").getFile();
        Cnab400Convenio6Processador processador = new Cnab400Convenio6Processador(arquivoRetorno);
        GerenciadorProcessoAbstract gerenciadorProcesso = new Cnab400Convenio6GerenciadorProcesso(processador, new Cnab400ProcessadorRetorno());
        RetornoInterface retorno = gerenciadorProcesso.processar();
        Assert.assertNotNull(retorno);
    }

    @Test
    public void deveProcessarUmArquivoCnab400Conv6UsandoFactory() throws Exception {
        File arquivoRetorno = getApplicationContext().getResource("classpath:ret/400/CNAB400-CONV6-TEST-001.ret").getFile();
        GerenciadorProcessoAbstract gerenciadorProcesso = GerenciadorProcessoFactory.getProcessadorArquivo(arquivoRetorno);
        Assert.assertThat(gerenciadorProcesso, CoreMatchers.instanceOf(Cnab400Convenio6GerenciadorProcesso.class));
        RetornoInterface retorno = gerenciadorProcesso.processar();
        Assert.assertNotNull(retorno);
    }

    @Test
    public void deveProcessarUmArquivoCnab400Conv7() throws Exception {
        File arquivoRetorno = getApplicationContext().getResource("classpath:ret/400/CNAB400-CONV7-TEST-001.ret").getFile();
        Cnab400Convenio7Processador processador = new Cnab400Convenio7Processador(arquivoRetorno);
        GerenciadorProcessoAbstract gerenciadorProcesso = new Cnab400Convenio7GerenciadorProcesso(processador, new Cnab400ProcessadorRetorno());
        RetornoInterface retorno = gerenciadorProcesso.processar();
        Assert.assertNotNull(retorno);
    }

    @Test
    public void deveProcessarUmArquivoCnab400Conv7UsandoFactory() throws Exception {
        File arquivoRetorno = getApplicationContext().getResource("classpath:ret/400/CNAB400-CONV7-TEST-001.ret").getFile();
        GerenciadorProcessoAbstract gerenciadorProcesso = GerenciadorProcessoFactory.getProcessadorArquivo(arquivoRetorno);
        Assert.assertThat(gerenciadorProcesso, CoreMatchers.instanceOf(Cnab400Convenio7GerenciadorProcesso.class));
        RetornoInterface retorno = gerenciadorProcesso.processar();
        Assert.assertNotNull(retorno);
    }

}
